<?php include('header.php'); ?>

<!-- RCB Branch Stylesheet -->
<link href="css/branch.css" rel="stylesheet">

<header class="page-header about">

  <div class="container">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400">RCB Branch Location</h4>

        <h2 class="fw-500 brand-grey-c"><span class="brand-primary-mid-c">4224 SE Adams Rd.</h2>

        <h4>Bartlesville, Ok</h4>

      </div>

    </div>

  </div>

</header>

<div class="container sm-mt-0 md-mt-3 lg-mt-3">

	<div class="row">

		<div class="col-lg-4 col-lg-push-8 branch-sidebar">

			<div class="row">

				<img class="img-responsive hidden-lg" src="images/hero-green-lg.jpg">

				<a role="button" class="btn btn-branch-sidebar" href=""><i class="fa fa-map-signs" aria-hidden="true"></i> Get Directions</a><a role="button" class="btn brand-grey btn-branch-sidebar" href=""><i class="fa fa-print" aria-hidden="true"></i> Print This Page</a>

				<div class="col-lg-12 branch-details">
					<ul class="list-unstyled pt-1 mb-0">
						<li><i class="fa fa-map-marker" aria-hidden="true"></i> 4224 SE Adams Rd. Bartlesville, OK 74006</li>
						<li><i class="fa fa-phone" aria-hidden="true"></i> 918.337.1362</li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i> bartlesville@rcbbank.com</li>
					</ul>
				</div>

				<div class="col-lg-12 branch-hours">
					<ul class="list-unstyled mb-0">
						<li><h5 class="fw-600">Lobby Hours</h5></li>
						<li><span class="hours-days">Mon &ndash; Fri:</span><span class="hours-hours">9am &ndash; 6pm</span></li>
						<li><span class="hours-days">Mon &ndash; Fri:</span><span class="hours-hours">9am &ndash; 6pm</span></li>
						<li><h5 class="fw-600">Drive-Thru Hours</h5></li>
						<li><span class="hours-days">Mon &ndash; Fri:</span><span class="hours-hours">7am &ndash; 6pm</span></li>
						<li><span class="hours-days">Mon &ndash; Fri:</span><span class="hours-hours">8am &ndash; 12pm</span></li>
					</ul>
				</div>

				<div class="col-lg-12 mb-2">
					<div id="map"></div>
				</div>

				<div class="col-lg-12 branch-services pb-1">
					<h4 class="fw-600">Branch Services</h4>
					<div class="flex"><ul class="list-unstyled mb-0"><li><b>Bank</b></li><li><a href="">Checking</a></li><li><a href="">Savings</a></li><li><a href="">ATM/Debit Card</a></li><li><a href="">Credit Card</a></li></ul><ul class="list-unstyled mb-0"><li><b>Borrow</b></li><li><a href="">Mortgage</a></li><li><a href="">Consumer Loans</a></li><li><a href="">Home Equity</a></li></ul><ul class="list-unstyled mb-0"><li><b>Invest</b></li><li><a href="">CD's &amp; IRA's</a></li><li><a href="">Trust Services</a></li><li><a href="">Wealth Management</a></li></ul><ul class="list-unstyled mb-0"><li><b>Manage</b></li><li><a href="">Checking</a></li><li><a href="">Savings</a></li><li><a href="">ATM/Debit Card</a></li></ul></div>
				</div>

			</div>

		</div>

		<div class="col-lg-8 col-lg-pull-4 branch-about">
			<img class="img-responsive hidden-xs hidden-sm hidden-md" src="images/hero-green-lg.jpg">
			<h4 class="mt-2">About This Branch</h4>

			<div class="row">

				<div class="col-xs-12 col-sm-6">
					<div id="staffOne" class="carousel staff-carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class=" carousel-indicators">
							<li data-target="#staffOne" data-slide-to="0" class="active"></li>
							<li data-target="#staffOne" data-slide-to="1"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<div class="staff-card">
									<div class="staff-hdr">
										<div class="staff-bg"></div>
										<img class="profile-pic" src="images/staff.jpg" alt="staff member">
									</div>
									<div class="staff-card-body">
										<h3>Chriss Thompson</h3>
										<h5>Vice President of Client Strategy<br />2019 N. Summit Arkansas City</h5>
										<a href="" class="btn btn-brand-alt mb-sm-2 mt-1">Contact Chriss</a>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
									<div class="staff-card-body about-card-body">
										<h3>Chriss Thompson</h3>
										<h5>Vice President of Client Strategy</h5>
										<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
									</div>
								</div>
							</div>
						</div>
						<!-- Controls -->
						<a class="left carousel-control" href="#staffOne" role="button" data-slide="prev">
						<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#staffOne" role="button" data-slide="next">
						<span class="sr-only">Next</span>
						</a>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6">
					<div id="staffTwo" class="carousel staff-carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class=" carousel-indicators">
							<li data-target="#staffTwo" data-slide-to="0" class="active"></li>
							<li data-target="#staffTwo" data-slide-to="1"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<div class="staff-card">
									<div class="staff-hdr">
										<div class="staff-bg"></div>
										<img class="profile-pic" src="images/staff.jpg" alt="staff member">
									</div>
									<div class="staff-card-body">
										<h3>Chriss Thompson</h3>
										<h5>Vice President of Client Strategy<br />2019 N. Summit Arkansas City</h5>
										<a href="" class="btn btn-brand-alt mb-sm-2 mt-1">Contact Chriss</a>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
									<div class="staff-card-body about-card-body">
										<h3>Chriss Thompson</h3>
										<h5>Vice President of Client Strategy</h5>
										<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
									</div>
								</div>
							</div>
						</div>
						<!-- Controls -->
						<a class="left carousel-control" href="#staffTwo" role="button" data-slide="prev">
						<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#staffTwo" role="button" data-slide="next">
						<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>

			<p class="mt-2 mb-2">We aim to build a stronger community by helping local businesses with thier financing and cash management needs. We help build good neightborhoods by making it possible for homeowners to buy, build or renovate property.</p>
		
	        <div class="row flex mt-1">
	          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-2">
	            <div class="card">
	              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
	              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
	            </div>
	          </div>

	          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-2">
	            <div class="card">
	              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
	              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
	            </div>
	          </div>

	          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-2">
	            <div class="card">
	              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
	              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
	            </div>
	          </div>
	          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-2">
	            <div class="card">
	              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
	              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
	            </div>
	          </div>

	          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-2">
	            <div class="card">
	              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
	              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
	            </div>
	          </div>

	          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-0">
	            <div class="card">
	              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
	              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
	            </div>
	          </div>
	     	</div>
	        <div class="row">
	          <div class="col-lg-12">
	            <h4><a class="brand-primary-mid-c" href="/learing-center">Visit the Learning Center &raquo;</a></h4>
	          </div>
	        </div>
		</div>

	</div>

</div>

<?php include('footer.php') ?>

<script>

// Creates the map

var map, infowindow, gmarkers = [];

function initMap() {

	var location = [['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519"><b>Bartlesville Office</b></a><br />4224 SE Adams Rd.<br />Bartlesville, OK 74006</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:9183371362">918.337.1362</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519">Get Directions</a></div>', 36.7430386, -95.9312459, 1]];
	var centerLatLng = {lat: 36.7430386, lng: -95.9312459};
	var mapCanvas = document.getElementById('map');
	var mapOptions = {
		center: centerLatLng,
		zoom: 15,
		mapTypeControl: false,
		gestureHandling: 'cooperative',
		disableDefaultUI: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	map = new google.maps.Map(mapCanvas, mapOptions);
	infowindow = new google.maps.InfoWindow();
	createMarkers(location,infowindow);

}

function createMarkers(loc,infowin) {

	var marker, i;
	var numberMarkerImg = {
		url: 'http://golocaldev.com/rcb/mocks/images/pin.png',
		size: new google.maps.Size(45, 45),
		scaledSize: new google.maps.Size(36, 36),
		labelOrigin: new google.maps.Point(18, 18)
	};

	for (i = 0; i < loc.length; i++) {
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(loc[i][1], loc[i][2]),
			title: 'Click for more info',
			label: {text: (i+1).toString(), color: '#ffffff' }, // adds a label to the facility marker and changes the text to white
			icon: numberMarkerImg, // adds a custom marker image
			map: map
		});

		google.maps.event.addListener(marker, 'click', (function (marker, i) {
			return function () {
				infowin.setContent(loc[i][0]);
				infowin.open(map, marker);
			}
		})(marker, i));

		gmarkers.push(marker);
	}
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJz0Oxwx75_kS1j3J67KV_FsWsT8kxpFA&callback=initMap"></script>
