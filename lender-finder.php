<?php include('header.php'); ?>

<!-- RCB Blog Stylesheet -->
<link href="css/bootstrap-slider.min.css" rel="stylesheet">

<!-- RCB Blog Stylesheet -->
<link href="css/location-finder-alt.css" rel="stylesheet">

	<?php include('inc/lender-search.php'); ?>

	<section class="list-wrapper m-0 pt-1 pl-1 pr-1 pb-1">

		<div class="container-fluid">

			<div class="row">

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Brynn Defenbaugh</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Brynn Defenbaugh</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Brynn Defenbaugh</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Brynn Defenbaugh</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Brynn Defenbaugh</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Jill Klynn</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>John Baker</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="contact-list-table">
						<div class="staff-hdr">
							<div class="staff-bg"></div>
							<img class="profile-pic" src="images/staff.jpg" alt="staff member">
						</div>
						<div class="contact-list-table-cell">
							<h4>Goofy Frogs</h4>
							<p>Vice President of Client Strategy<br />2019 N. Summit<br />Arkansas City, KS 67005</p>
						</div>
						<div class="contact-list-table-cell">
							<ul class="list-unstyled">
								<!-- inline styles to fix obnoxious IE bug -->
								<li><span class="fw-600">MLO#</span> 832625<hr></li>
								<li><span class="fw-600">Office:</span> 620-441-2198<hr></li>
								<li><span class="fw-600">Fax:</span> 620-441-2198<hr></li>
								<li><a href="#contact" role="button" data-toggle="modal" data-target="#contact">Contact Brynn</a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>

		</div>

	</section>

<?php include('footer.php') ?>

<!-- Bootsrtap slider -->
<script src="js/bootstrap-slider.min.js"></script>

<script>

	$( document ).ready(function() {

		resizeListItems()

		function resizeListItems() {
		    var maxHeight = -1;
		    //set all cards height to the tallest card
		    $(".contact-list-table").each(function() {
		      $(this).css({"height":"auto"});
		      maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
		    });
		    $(".contact-list-table").each(function() {
		      $(this).height(maxHeight);
		    });
	    }

		$(window).on('resize', function() {
			resizeListItems()
		})

	})

</script>
