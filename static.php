<?php include('header.php'); ?>

<section class="container-fluid hero brand-primary">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

        <h5>Category Here (if one exists)</h5>

        <h1><a class="brand-primary-white" href="blog.php">Static Page Headline Here</a></h1>

        <p>Static page hero text blurb. <a class="brand-primary-light-c" href="blog.php">Link in the text blurb</a>.</p>

      </div>

    </div>

  </div>

</section>

<nav class="page-nav brand-primary-mid">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <ul class="list-unstyled" role="tablist">
                  <li role="presentation"><button class="btn fw-600 ml-05">Explore &nbsp; <i class="fa fa-angle-right fw-600" aria-hidden="true"></i></button></li>
                  <li role="presentation" class="fw-600 explore">Explore <i class="fa fa-angle-right" aria-hidden="true"></i></li>
                  <li role="presentation" class="page-nav-nav"><a href="static-content" data-scrollto=".static-content" class="scrollto-btn">In Page Link</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="home.php">External Page link</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="hero" data-scrollto=".hero" class="scrollto-btn">In Page Link</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="home.php">External Page link</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="static-content" data-scrollto=".static-content" class="scrollto-btn">In Page Link</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="home.php">External Page link</a></li>
            </ul>
          </div>
        </div>
    </div>
</nav>

<div class="page-nav-placeholder"></div>

<header class="page-header">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400 brand-primary-mid-c">Section Header Category (if one exists)</h4>

        <h2 class="fw-500 brand-grey-c">Section Header <span class="brand-primary-mid-c">Headline Here</span></h2>

      </div>

    </div>

  </div>

</header>

<section class="static-content">

	<div class="container-fluid">

    	<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">

				<article>

					<h2 class="fw-300">Page Article Header Light</h2>

					<h2 class="fw-400">Page Article Header Medium</h2>

					<h2>Page Article Header Default</h2>

					<p class="lead mt-1 mb-1">Paragraph lead in copy veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>

					<h3 class="mb-1">Page Article Subheader</h3>

					<p class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<h4 class="mb-1">Page Article Subheader</h4>

					<p class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<button class="btn btn-brand btn-lg mt-1">Learn More</button>

					<button class="btn btn-brand-alt btn-lg mt-1">Learn More</button>

				</article>

			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">

				<article>

					<h2 class="fw-300 brand-primary-c">Page Article Brand Green</h2>

					<h2 class="fw-400 brand-primary-c">Page Article Brand Green</h2>

					<h2 class="brand-primary-c">Page Article Brand Green</h2>

					<p class="lead brand-primary-c mt-1 mb-1">Paragraph lead in copy veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>

					<h3 class="mb-1 brand-primary-c">Page Article Subheader Brand Green</h3>

					<p class="mb-1 brand-primary-c">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<h4 class="mb-1 brand-primary-c">Page Article Subheader Brand Green</h4>

					<p class="mb-1 brand-primary-c">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				</article>

			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">

				<article>

					<h2 class="fw-300 brand-primary-mid-c">Page Article Mid Green</h2>

					<h2 class="fw-400 brand-primary-mid-c">Page Article Mid Green</h2>

					<h2 class="brand-primary-mid-c">Page Article Mid Green</h2>

					<p class="lead brand-primary-mid-c mt-1 mb-1">Paragraph lead in copy veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>

					<h3 class="mb-1 brand-primary-mid-c">Page Article Subheader Mid Green</h3>

					<p class="mb-1 brand-primary-mid-c">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<h4 class="mb-1 brand-primary-mid-c">Page Article Subheader Mid Green</h4>

					<p class="mb-1 brand-primary-mid-c">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				</article>

			</div>

		</div>

	</div>

</section>


<?php include('footer.php') ?>
