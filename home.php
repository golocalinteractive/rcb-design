<?php include('header.php'); ?>

    <?php include('inc/alert.php'); ?>

    <section class="container-fluid hero hero-home">

		<div class="overlay"></div>

		<div class="new-mobile hidden-md hidden-lg">
			<img src="images/new-arrow-mobile.png">
			<p class="fw-600">Welcome to our new website.<br />To bank online, <span class="fw-800">sign in here.</span></p>
		</div>

		<div class="hero-content hero-table">

				<div class="hero-cell">

					<h1 class="brand-primary-white">RCB Bank Homepage Headline.</h1>

					<p class="brand-primary-white">Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat.</p>

					<button class="btn btn-brand-on-light-green btn-lg mt-1">Find a Branch</button>

				</div>

				<div class="homepage-signin-wrapper hero-cell hidden-xs hidden-sm">

					<div class="new-desktop">
						<p class="fw-600">Welcome to our new website.<br />To bank online, <span class="fw-800">sign in here.</span></p>
						<img src="images/new-arrow-desktop.png">
					</div>

					<div class="homepage-signin pb-1 pl-2 pr-2 pt-1 mr-1">
						<h4 class="brand-primary-c">Welcome</h4>
						<form>
							<div class="form-group">
								<input type="text" class="form-control" id="userName" placeholder="Username">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" id="userName" placeholder="Password">
							</div>
							<div class="checkbox brand-grey-c">
								<label>
									<input type="checkbox"> Remember Me?
								</label>
							</div>
							<button class="btn btn-brand-alt btn-lg btn-block mb-1">Sign In</button>
						</form>
					</div>

				</div>

		</div>

	</section>

	<section class="brand-primary inner-shadow-on-dark-green ml-0 mr-0 pt-1 pb-1">

		<div class="container-fluid">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 vertical-center">

					<h3 class="fw-500 brand-primary-white m-0 pt-xs-075 pb-xs-0 sm-pt-075 sm-pb-0">Browse <span class="brand-primary-light-c">Our Products</span></h3>
					<p class="mt-1 pb-xs-1 sm-pb-1 fw-600 hidden-md"><span class="brand-primary-white">Not sure which product you need? Try our</span> <a class="brand-primary-light-c" href="account-finder.php">Account Finder</a><span class="brand-primary-white">.</span></p>

				</div>

			</div>

		</div>

	</section>

    <section class="pb-2 ml-0 mr-0 section-wrapper">

        <?php include('inc/product-carousel.php'); ?>

    </section>


	<section class="product-wrapper home-product-wrapper m-0 pl-1 pt-2 pb-2">

		<div class="container">

			<div class="row">

				<div class="col-sm-6 col-md-6 col-lg-6">

					<div class="product-card">
						<div class="product-icon-wrapper" href=""><span class="product-icon product-icon-privacy-security"></span></div>
						<div class="product-card-body mt-2">
							<h3 class="fw-600 brand-primary-mid-c">Privacy Header</h3>
							<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
							<a role="button" class="btn btn-brand-alt btn-lg mt-1">Privacy Policy</a>
						</div>
					</div>

				</div>

				<div class="col-sm-6 col-md-6 col-lg-6">

					<div class="product-card product-card-alt mt-xs-2">
						<div class="product-icon-wrapper" href=""><span class="product-icon product-icon-overdraft-protection"></span></div>
						<div class="product-card-body mt-2">
							<h3 class="fw-600 brand-primary-mid-c">Security Header</h3>
							<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
							<a role="button" class="btn btn-brand-alt btn-lg mt-1">Online Security</a>
						</div>
					</div>

				</div>

			</div>

		</div>

	</section>

    <section class="callout callout-photo mt-2 mb-2">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <div class="callout-body brand-primary clearfix">

              <div class="callout-cell photo-cell"><img class="img-responsive" src="images/teen-money-md.jpg"></div>

              <div class="callout-cell">
              	<h2 class="mb-03"><b><a class="brand-primary-white" href="/teen-money-smart">Is Your Teen Money&ndash;Smart?</a></b>
              	<h3 class="fw-400 brand-primary-white">Test their skills with these 6 tips</h3>
              </div>

              <div class="callout-cell hidden-xs hidden-sm">

              	<a href=""><i class="fa fa-angle-right fa-4x brand-primary-white" aria-hidden="true"></i></a>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

    <header class="page-header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-grey-c">RCB Bank <span class="brand-primary-mid-c">Learning Center</span></h2>

          </div>

        </div>

      </div>

    </header>

    <section class="learning-center mt-1 mb-1">

      <div class="container-fluid">

        <div class="row flex mt-1">

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

        </div><!-- /row -->

        <div class="row">

          <div class="col-lg-12">

            <h4><a class="brand-primary-mid-c" href="/learing-center">Visit the Learning Center &raquo;</a></h4>

          </div>

        </div>

      </div>

    </section>


<?php include('footer.php') ?>