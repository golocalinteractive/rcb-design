<?php include('header.php'); ?>

<!-- RCB Blog Stylesheet -->
<link href="css/bootstrap-slider.min.css" rel="stylesheet">

<!-- RCB Blog Stylesheet -->
<link href="css/location-finder-alt.css" rel="stylesheet">

	<div class="locator">

		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"><h4 class="brand-primary-c fw-500 locator-title">Find a branch near you</h4><h4 class="fw-500 search-by-lender">or <a href="">Search by Lender</a></h4></div>

				<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">

					<div class="finder-form">
						<div class="input-group find-a-branch-search">
							<div class="inner-addon left-addon">
								<input type="text" class="form-control" placeholder="City, State or Zip">
							</div>
							<span class="input-group-btn">
								<button class="btn brand-primary-mid brand-primary-white" type="button">Search</button>
							</span>
						</div><!-- /input-group -->
					</div>

				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 mt-xs-1">

					<div class="btn-group btn-block">
						<button class="btn btn-brand-alt btn-block btn-lg dropdown-toggle btn-location-filter" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Filter by <span class="glyphicon glyphicon-chevron-down"></span>
						</button>
						<ul class="loc-list-dropdown dropdown-menu pb-2">
							<h5 class="fw-600 ml-1">Distance</h5>
							<li class="pl-1 pr-1"><div class="row"><div class="col-sm-4"><small>10mi</small></div><div class="col-sm-4 center-aligned"><small>50mi</small></div><div class="col-sm-4 right-aligned"><small>100mi</small></div></div></li>
							<li class="pl-1 pr-1"><input id="locationSlider" data-slider-id='locationSlider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/></li>
							<!--<li><span id="locationSliderCurrentSliderValLabel">Current Slider Value: <span id="locationSliderVal">3</span></span></li>-->
							<div class="col-xs-12"><h5 class="fw-600 mt-1">Account Services</h5></div>
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
									<input type="checkbox" value="">Checking</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Savings
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Home Loans
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Consumer Loans
									</label>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Lost or Stolen Card
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Reorder Checks
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Business Services
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">EMV Chip Card
									</label>
								</div>
							</div>
							<div class="col-xs-12"><h5 class="fw-600 mt-1">ATM Services</h5></div>
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">No Charge ATM
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Account Balance
									</label>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Transfer Funds
									</label>
								</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="">Cash Withdrawals
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 mt-1"><button class="btn btn-brand-alt btn-block">Apply filters</button></div>
						<div class="col-xs-12 col-sm-6 mt-1"><button class="btn btn-brand-alt btn-block">Clear filters</button></div>
						</ul>
					</div><!--/ locator -->

				</div>

			</div>

			<div class="row">

				<div class="col-xs-12 col-md-6 col-md-offset-3">

					<!-- Nav tabs -->
					<ul class="nav loc-tabs nav-tabs" role="tablist">
						<li class="list-tab-wrapper active" role="presentation"><a href="#listPanel" aria-controls="listPanel" role="tab" data-toggle="tab">Grid View</a></li>
						<li class="map-tab-wrapper" role="presentation"><a href="#mapPanel" aria-controls="mapPanel" role="tab" data-toggle="tab">Map View</a></li>
			  		</ul>

				</div>

			</div>

		</div>

	</div>

	<div class="tab-content">

		<div role="tabpanel" id="listPanel" class="locations tab-pane active">

			<div class="m-0 pl-1 pr-1 pb-2">

				<div class="container-fluid">

					<div class="row">

						<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
							<div id="staffOne" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffOne" data-slide-to="0" class="active"></li>
									<li data-target="#staffOne" data-slide-to="1"></li>
									<li data-target="#staffOne" data-slide-to="2"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php"><span class="instabank"></span> Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" data-target="#staffOne" data-slide-to="1" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" data-target="#staffOne" data-slide-to="2" class="pb-05 inline-block">Lender</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card center-aligned">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<a class="btn btn-brand-alt" data-target="#staffOne" data-slide-to="2" href="#">Lender</a>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<img class="img-responsive width-100" src="images/staff-placeholder.jpg" alt="...">
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
												<h5>Vice President of Lending Services</h5>
												<ul class="list-unstyled">
													<li><a href="branch_details.php"><small>Bartlesville, OK</small></a></li>
													<li><a href="branch_details.php"><small>Blackmore, OK</small></a></li>
													<li><a href="branch_details.php"><small>Norman, OK</small></a></li>
												</ul>
												<a class="btn btn-brand-alt" href="branch-details.php">Contact Chriss</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffOne" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffOne" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
							<div id="staffTwo" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffTwo" data-slide-to="0" class="active"></li>
									<li data-target="#staffTwo" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<div class="staff-card-body-header">
													<div class="staff-card-body-table-cell">
														<img class="bank-anywhere-icon" src="images/bank_anywhere_icon.png">
													</div>
													<div class="staff-card-body-table-cell">
														<h3 class="brand-primary-mid-c"><a href="branch-detail.php"><span class="instabank"></span> Bartlesville Branch</a></h3>
														<h5>4224 SE Adams Rd. 74006</h5>
													</div>
												</div>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffTwo" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffTwo" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
							<div id="staffThree" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffThree" data-slide-to="0" class="active"></li>
									<li data-target="#staffThree" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php">Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffThree" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffThree" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
							<div id="staffFour" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffFour" data-slide-to="0" class="active"></li>
									<li data-target="#staffFour" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php">Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffFour" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffFour" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>


						<!-- REPEAT OF THE CAROUSELS ABOVE WITH DIFFERENT ID'S-->

						<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
							<div id="staffFive" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffFive" data-slide-to="0" class="active"></li>
									<li data-target="#staffFive" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php">Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffFive" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffFive" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
							<div id="staffSix" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffSix" data-slide-to="0" class="active"></li>
									<li data-target="#staffSix" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php">Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffSix" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffSix" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
							<div id="staffSeven" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffSeven" data-slide-to="0" class="active"></li>
									<li data-target="#staffSeven" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php">Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffSeven" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffSeven" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
							<div id="staffEight" class="carousel staff-carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class=" carousel-indicators">
									<li data-target="#staffEight" data-slide-to="0" class="active"></li>
									<li data-target="#staffEight" data-slide-to="1"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="staff-card">
											<a href=""><img class="img-responsive width-100" src="images/hero-green-lg.jpg" alt="..."></a>
											<div class="staff-card-body">
												<h3 class="brand-primary-mid-c"><a href="branch-detail.php">Bartlesville Branch</a></h3>
												<h5>4224 SE Adams Rd. 74006</h5>
												<ul class="list-unstyled">
													<li><a href="" class="pb-05 pt-1 inline-block">(918) 337-1362</a></li>
													<li><a href="" class="pb-05 inline-block">Hours</a></li>
													<li><a href="" class="pb-05 inline-block">Lenders</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="staff-card">
											<ul class="list-unstyled location-hours">
												<li class="fw-600 brand-primary-c">Lobby Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
												<li class="fw-600 brand-primary-c mt-1">Drive-Thru Hours</li>
												<li>Monday - Fri <span>8:00AM – 6:00PM</span></li>
												<li>Sat <span>8:00AM – 6:00PM</span></li>
												<li>Sun <span>Closed</span></li>
											</ul>
											<div class="btn-card-wrapper">
												<a role="button" class="btn btn-brand btn-card btn-lg btn-block" href="branch-detail.php">Branch Details &rarr;</a>
											</div>
										</div>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#staffEight" role="button" data-slide="prev">
								<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#staffEight" role="button" data-slide="next">
								<span class="sr-only">Next</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" id="mapPanel" class="location-map tab-pane"><div id="map"></div></div>
	</div>

<?php include('footer.php') ?>

<!-- Bootsrtap slider -->
<script src="js/bootstrap-slider.min.js"></script>

<script>

	function resizeCard() {
	    var maxHeight = -1;
	    // unhide all inactive cards to get their height
	    $(".item").each(function() {
	      if(!($(this).hasClass("active"))) {
	        $(this).css({"display":"block"});
	      }
	    })
	    //set all cards height to the tallest card
	    $(".staff-card, .product-card").each(function() {
	      $(this).css({"height":"auto"});
	      maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
	    });
	    $(".staff-card, .product-card").each(function() {
	      $(this).height(maxHeight);
	    });
	    // hide all inactive cards again
	    $(".item").each(function() {
	      if(!($(this).hasClass("active"))) {
	        $(this).css({"display":""});
	      }
	    })
    }

	$('.list-tab-wrapper').on('shown.bs.tab', function (e) {
		e.target // newly activated tab
		e.relatedTarget // previous active tab
		resizeCard()
	})

/****** Bootstrap slider http://seiyria.com/bootstrap-slider/ */

$("#locationSlider").slider();

// Creates the map

var map, bounds, infowindow, gmarkers = [];

$('#mapPanel, #map').css('height', ($(window).height() - $('#map').offset().top))

$(window).on('load resize', function() {

	var currCenter = map.getCenter();
	google.maps.event.trigger(map, 'resize');
	map.setCenter(currCenter);

})

$('.map-tab-wrapper').on('shown.bs.tab', function (e) {
	e.target // newly activated tab
	e.relatedTarget // previous active tab
	$('#mapPanel, #map').css('height', ( $(window).height() - $('#map').offset().top ) )
	var currCenter = map.getCenter();
	google.maps.event.trigger(map, 'resize');
	map.setCenter(currCenter);
})

function initMap() {


	var location = [['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519"><b>Bartlesville Office</b></a><br />4224 SE Adams Rd.<br />Bartlesville, OK 74006</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:9183371362">918.337.1362</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519">Get Directions</a></div>', 36.7430386, -95.9312459, 1]];

	var locations = [
        ['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,7z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519"><b>Bartlesville Office</b></a><br />4224 SE Adams Rd.<br />Bartlesville, OK 74006</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:9183371362">(918) 337-1362</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519">Get Directions</a></div>', 36.7430386, -95.9312459, 1],
		['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.0361452,-97.0993693,7z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0x9861631061616773!8m2!3d36.8120334!4d-97.3005867"><b>Blackwell Office</b></a><br />1350 W Doolin Ave.<br />Blackwell, OK 74631</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:5803630005">(580) 363-0005</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.0361452,-97.0993693,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0x9861631061616773!8m2!3d36.8120334!4d-97.3005867">Get Directions</a></div>', 36.8120334, -97.3005867, 2],
		['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.1757256,-96.9835562,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xc6760e3db6180696!8m2!3d36.1180476!4d-97.0630074"><b>Stillwater Office</b></a><br />324 S Duck St.<br />Stillwater, OK 74074</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:4053777600">(405) 377-7600</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.1757256,-96.9835562,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xc6760e3db6180696!8m2!3d36.1180476!4d-97.0630074">Get Directions</a></div>', 36.1180476, -97.0630074, 3],
		['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@35.7622672,-97.043981,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xb0d48bd0fc1ba4e8!8m2!3d35.262861!4d-97.4883842"><b>Norman Office</b></a><br />3151 W Tecumseh Rd #200<br />Norman, OK 73072</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:5803630005">(580) 363-0005</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@35.7622672,-97.043981,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xb0d48bd0fc1ba4e8!8m2!3d35.262861!4d-97.4883842">Get Directions</a></div>', 35.262861, -97.4883842, 4],
    ];	
    var centerLatLng = {lat: 36.7430386, lng: -95.9312459};
	var mapCanvas = document.getElementById('map');
	var mapOptions = {
		center: centerLatLng,
		zoom: 9,
		mapTypeControl: false,
		disableDefaultUI: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	map = new google.maps.Map(mapCanvas, mapOptions);

	//create empty LatLngBounds object
	bounds = new google.maps.LatLngBounds();

	infowindow = new google.maps.InfoWindow();
	createMarkers(locations,infowindow);

}

function createMarkers(loc,infowin) {

	var marker, i;
	var numberMarkerImg = {
		url: 'http://golocaldev.com/rcb/mocks/images/pin.png',
		size: new google.maps.Size(45, 45),
		scaledSize: new google.maps.Size(36, 36),
		labelOrigin: new google.maps.Point(18, 18)
	};

	for (i = 0; i < loc.length; i++) {
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(loc[i][1], loc[i][2]),
			title: 'Click for more info',
			label: {text: (i+1).toString(), color: '#ffffff' }, // adds a label to the facility marker and changes the text to white
			icon: numberMarkerImg, // adds a custom marker image
			map: map
		});

		//extend the bounds to include each marker's position
  		bounds.extend(marker.position);

		google.maps.event.addListener(marker, 'click', (function (marker, i) {
			return function () {
				infowin.setContent(loc[i][0]);
				infowin.open(map, marker);
			}
		})(marker, i));

		gmarkers.push(marker);
	}

	//now fit the map to the newly inclusive bounds
	map.fitBounds(bounds);

	//(optional) restore the zoom level after the map is done scaling
	var listener = google.maps.event.addListener(map, "idle", function () {
	    map.setZoom(7);
	    google.maps.event.removeListener(listener);
	});
}

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJz0Oxwx75_kS1j3J67KV_FsWsT8kxpFA&callback=initMap"></script>

