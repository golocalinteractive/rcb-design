(function ($) {

  $(document).ready(function() {

    //init carousels

    $(".carousel").carousel({
      interval: false,
    })

    // Adds touch support to carousels via bcSwipe.js

    $(".carousel").bcSwipe({ threshold: 25 });


    // faq collapse state changes, panels are specific to BS 3* accordion collapse events

    $("#faqAccordion .panel").on("show.bs.collapse", function (e) {
        $(e.currentTarget).addClass("shown");
    })

    $("#faqAccordion .panel").on("hide.bs.collapse", function (e) {
        $(e.currentTarget).removeClass("shown");
    })

    // init tooltips

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    // 3rd tier submenu control for mobile nav -- does not come with Bootstrap
    $(".level3 .dropdown-toggle").click(function(e) {
      $(this).closest("li").toggleClass("open")
      e.stopPropagation(); //stops from hiding menu
      e.preventDefault();
    })


    /***** Header Navigation *******

    This extends cbpHorizontalSlideOutMenu.js and Bootstrap Tabs.
    https://tympanus.net/codrops/2013/05/17/horizontal-slide-out-menu/
    https://getbootstrap.com/docs/3.3/javascript/#tabs

    Resizes the nav submenu background when a tab is clicked.
    The plugin has to be extended to resize the background, because
    it was only originally being resized when top level nav is clicked.
    Tab content needs to be fluid and as such needs to be abe to resize
    the background.

    */

    // Create the header navigation object

    var menu = new cbpHorizontalSlideOutMenu( document.getElementById("cbp-hsmenu-wrapper") );

    function resizeNav() {
      $(".cbp-hsmenubg").css("height", $(".cbp-hssubmenu").height());
    }

    // Manages and resets the tabs and tab contents when a top nav link is clicked

    function menuReset(tabcontent, tablist) {

      $(".cbp-hssubmenu .tab-pane").each(function() {
        $(this).removeClass("in active")
      })

      $(tabcontent + " " + ".tab-pane").first().addClass("in active");

      $(".cbp-hssubmenu .tablist li").each(function() {
        if( $(this).hasClass("active") ) {
          $(this).removeClass("active")
        }
      })

      $(tablist + " " + "li").first().addClass("active");

    }

    $('.cbp-hssubmenu a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
      e.target // newly activated tab
      var cur = e.relatedTarget // previous active tab
      resizeNav()
    })

    $(".top-nav-business").on("click touchstart", function() {
      menuReset(".business-tab-content", ".nav-business-tablist")
    })

    $(".top-nav-personal").on("click touchstart", function() {
      menuReset(".personal-tab-content", ".nav-personal-tablist")
    })


    function toggleDesktopNav() {

      var allNavs = $(".cbp-hssubmenu .tab-content > .tab-pane");

      $(allNavs).each(function() {
          $(this).removeClass("subnav-toggle-in");
          menu._openMenu( this, event );
      })

    }

    // Turns off the nav when they click outside of it

    $(".nav-backdrop").on("click touchstart", function() {
      toggleDesktopNav()
      menuReset(".personal-tab-content", ".nav-personal-tablist")
      menuReset(".business-tab-content", ".nav-business-tablist")
      $(this).removeClass("on")
    })

    // Turns on or off the nav backdrop (desktop only at the moment)

    $(".top-nav").on("click touchstart", function() {

      // is the window width large enough to show the desktop nav?

      if( $(window).width() > 991 ) {

        if( $(this).parent().hasClass("cbp-hsitem-open") ) {
          $(".nav-backdrop").addClass("on");
          // console.log("true")
        }
        else {
          $(".nav-backdrop").removeClass("on")
          // console.log("false")
          toggleDesktopNav()
        }
      }
    })

    /***** Modal Input Auto Focus *******

    These functions auto focus modal inputs when a modal is shown
    (for the search and login modals). The input element tagged
    with the html5 "autofocus" is focused. The form elements are
    cleared if the modal has been closed.

    */

    // Autofocus an input field in a modal if it has an autofocus element
    // https://stackoverflow.com/questions/14940423/autofocus-input-in-twitter-bootstrap-modal#23921617
    $(".modal").on("shown.bs.modal", function() {
      $(this).find("[autofocus]").focus();
    });

    // Clear form elements in modal after the modal has been closed
    // https://stackoverflow.com/questions/31022950/how-clear-bootstrap-modal-on-hide
    $(".modal").on("hidden.bs.modal", function () {
        $(this).find("input,textarea,select").val("").end();
    });

    /******* Smooth Scroll to an In-Page Section *******

    This implements a generic event handler for in-page buttons that smooth
    scrolls to their in-page content. This is generic enough to handle any
    in-page smooth scrolling. Add the "scrollto-btn" class to
    your button or link and give it a data-scrollto="#something".

    example:
    <button data-scrollto=".categories" class="scrollto-btn">View Products</button>

    */

    function stickyNav(value, e) {

      e.preventDefault();

      // get the body top padding (if any) to substract from the scroll distance

      var topPad = $("body").css("padding-top");
      topPad = parseInt(topPad, 10);

      if($(".page-nav").length) {

        var pagenavheight = $(".page-nav").height();

        $("html, body").animate({
            scrollTop: $(value).offset().top - (topPad + pagenavheight)
        }, 300);
      }

      else {
        $("html, body").animate({
            scrollTop: $(value).offset().top - topPad
        }, 300);
      }
    }

    $(".scrollto-btn").on("click", function(e) {
      var target = $(this).data("scrollto")
      if(target) {
        stickyNav(target, e);
      }
    });

    if($(".page-nav").length) {

      // get the width of the in-page nav

      var pagenavwidth = $(".page-nav ul").width() + 30;

      $(window).on("load resize", function() {

        var navheight = $(".nav-wrapper").height(); // console.log(navheight)// the global nav height
        var pagenavheight = $(".page-nav").height();  // console.log(pagenavheight) // the page nav height
        var pagenavpos, position;

        // scrolls the page to the content section specified in "value"

        function stickyPageNav(value, pos) {
          $("html, body").animate({
              scrollTop: $(value).offset().top - position
          }, 0);
        }

        // calculates the affix position for the affix() function

        function pageNavPos() {

          if( $(window).width() > 991 ) {
            $(".page-nav").css("top", navheight) // applies a "top" css property so when it"s affixed it will have the proper top position
            return ($(".page-nav").offset().top) - navheight // tells the affix function when to apply the .affix class
          }

          else {
            $(".page-nav").css("top", "0")
            return $(".page-nav").offset().top // tells the affix function when to apply the .affix class
          }

        }

        // collapse or expand (horizontally) the page nav based if its width exceeds window width

        if( $(this).width() <=  pagenavwidth) {
          // console.log("window is smaller")
          $(".page-nav").addClass("collapsed")
          $(".page-nav button").removeClass("hidden")
          $(".page-nav .explore").addClass("hidden")
        }

        else {
          // console.log("window is bigger")
          $(".page-nav").removeClass("collapsed")
          $(".page-nav button").addClass("hidden")
          $(".page-nav .explore").removeClass("hidden")
        }

        $(".page-nav").affix({

            offset: {
                top: pageNavPos()
            }
        });

        $(".page-nav").on("affixed.bs.affix", function(){
          $(".page-nav-placeholder").css({"height": pagenavheight, "display":"block"})
        });

        $(".page-nav").on("affixed-top.bs.affix", function(){
          $(".page-nav-placeholder").css("display","none")
        });

        $('.page-nav a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
          e.target // newly activated tab
          e.relatedTarget // previous active tab
          var href = $(this).attr("href");
          if( $(window).width() > 991 ) {
            position = navheight + pagenavheight;
          }
          else {
            position = pagenavheight;
          }
          stickyPageNav(href, position);
        })

      })

      // Controls whether the subnav is vertically collapsed or expanded based on a user clicking "explore"

      $(".page-nav a, .page-nav button").on("click", function() {

        if( $(".page-nav").hasClass("collapsed")) {
          var links = $(".page-nav-nav");
          links.each(function() {
            $(this).toggleClass("block");
          })
        }

      })

    }

    /******* Collapse/Expanse Footer All Programs *********/

    $(".footer-accordion-btn").on("click", function() {
      $("#footerProducts").toggleClass("hidden");
      $("span.accordion-plus-minus").toggleClass("accordion-plus-minus-btn-close");
    })

    /******* Collapse/Expand Mobile Navigation *********/

    $(".mobile-navbar-toggle").on("click", function() {
      $(this).toggleClass("is-active");
    })

    /******* Sets all staff card"s height to the tallest staff card *********/

    if($(".staff-card, .product-card").length) {
      $(window).on("load resize", function() {
        var maxHeight = -1;
        // unhide all inactive cards to get their height
        $(".item").each(function() {
          if(!($(this).hasClass("active"))) {
            $(this).css({"display":"block"});
          }
        })
        //set all cards height to the tallest card
        $(".staff-card, .product-card").each(function() {
          $(this).css({"height":"auto"});
          maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
        });
        $(".staff-card, .product-card").each(function() {
          $(this).height(maxHeight);
        });
        // hide all inactive cards again
        $(".item").each(function() {
          if(!($(this).hasClass("active"))) {
            $(this).css({"display":""});
          }
        })
      })
    }

    // Why RCB Bank page About Us Carousel : random brand background and color on cycle

    $("#whyRCBCarousel").on("slide.bs.carousel", function () {

    // hex colors for the slides, chosen at random

      var hexArray = ["#154734","#169a43","#6cc14a", "#646569"]
      var randomColor = hexArray[Math.floor(Math.random() * hexArray.length)];

      $("#whyRCBCarousel.carousel").css({backgroundColor: randomColor});

    });

    // images for the slideshow background container, chosen at random

    $("#whyRCBCarousel").on("slide.bs.carousel", function () {

      if($(window).width() > 500) {
        var imgArray = ["images/learning-center-lg.jpg","images/switch-kit-lg.jpg","images/cashback-checking-lg.jpg"]
      }

      else {
        var imgArray = ["images/why-rcb-sm-1.jpg","images/why-rcb-sm-2.jpg","images/why-rcb-sm-3.jpg"]
      }

      var randomImg = imgArray[Math.floor(Math.random() * imgArray.length)];
      // console.log(randomImg)
      $(".hero-why-rcb-carousel").css("background-image", "url(" + randomImg + ")");

    });

    // affixes the mobile nav tablist on scroll

    var blogtabsheight = $(".blog-tabs-wrapper").outerHeight();

    $(".blog-tabs-wrapper").affix({
      offset: {
        top: $(".nav-wrapper").height()
      }
    })

    $(".blog-tabs-wrapper").on("affixed.bs.affix", function(){
      $(".blog-tabs-placeholder").css({"height": blogtabsheight, "display":"block"})
    });

    $(".blog-tabs-wrapper").on("affixed-top.bs.affix", function(){
      $(".blog-tabs-placeholder").css("display","none")
    });

    // sets the height of the blog sidenav

    if($(".sidenav").length) {

      var tabs = $(".blog-content .nav-tabs li");
      var tab_panes = $(".blog-content .tab-pane");

      $(window).on("load resize", function() {

          var blog_aside = $(".sidenav")

          var blog_aside_height = $(window).height() - $(".nav-wrapper").height();

          blog_aside.css("height", blog_aside_height);

        if( $(window).width() > 991 ) {

          // if desktop or large tablet - show both tab panes

          tab_panes.each(function() {

            $(this).addClass("active")

          })
        }

        else {

          // are both tab panels active? If so, set one to active for mobile.

          if ( $("#read").hasClass("active") && $("#browse").hasClass("active") ) {

            $("#read").addClass("active");
            $(".read-tab-tab").addClass("active");
            $("#browse").removeClass("active");
            $(".browse-tab-tab").removeClass("active");

          }

        }

      })

    }

    // sets the height of the location finder sidenav

    if($(".loc-sidenav").length) {

      var tabs = $(".loc-content .nav-tabs li");
      var tab_panes = $(".loc-content .tab-pane");

      $(window).on("load resize", function() {

        if( $(window).width() > 767 ) {

          // if desktop or large tablet - show both tab panes

          tab_panes.each(function() {

            $(this).addClass("active")

          })

          var blog_aside = $(".loc-sidenav")

          var blog_aside_height = $(window).height() - $(".nav-wrapper").height();

          blog_aside.css("height", blog_aside_height);
        }

      })

    };

  });

}(jQuery));
