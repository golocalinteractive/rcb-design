<?php include('header.php'); ?>

<section class="container-fluid hero hero-personal-checking">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

        <h5>Contact Us</h5>

        <h1 class="brand-primary-white">We're Here to Help.</h1>

        <p class="mt-1 fw-400 p-large">Browse our list of FAQ's. Don't see what you're looking for? Give us a call or send us a message.</p>

      </div>

    </div>

  </div>

</section>

<div id="faq" class="bg-md-dk-blue brand-primary">

  <div class="container-fluid">

    <div class="row">

      <div class="col-xs-12 pl-0 pr-0">

        <div id="faqAccordion" data-children=".panel">
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion1" class="faq-a white pt-1 pb-1" aria-expanded="true" aria-controls="faqAccordion1"><span class="pb-2"><div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Branch Locations</span> <i class="fa fa-chevron-down brand-primary-mid-c" aria-hidden="true"></i></a>
            <div id="faqAccordion1" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion2" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion2">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Anytime Banker<i class="fa fa-chevron-down brand-primary-mid-c" aria-hidden="true"></i>
            </a>
            <div id="faqAccordion2" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, <a href="#">account finder</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion3" class=" faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion3">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Report a Lost/Stolen Card <i class="fa fa-chevron-down brand-primary-mid-c" aria-hidden="true"></i>
            </a>
            <div id="faqAccordion3" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion4" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion4">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Corporate Phone Numbers <i class="fa fa-chevron-down brand-primary-mid-c" aria-hidden="true"></i>
            </a>
            <div id="faqAccordion4" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion5" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion5">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Corporate Mailing Address <i class="fa fa-chevron-down brand-primary-mid-c" aria-hidden="true"></i>
            </a>
            <div id="faqAccordion5" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum <a href="facility.php" href="">Facility page</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>

        </div>

      </div>

    </div>

  </div><!--/ container -->

</div><!-- /FAQ -->


<div id="contact" class="brand-grey-light">

  <h4>Send us a message</h4>

  <form action="" class="contact-wrapper p-1 pb-2">

    <div class="container-fluid">

      <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="firstName">First Name</label>
            <input type="text" class="form-control input-lg" id="firstName" required>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="lastName">Last Name</label>
            <input type="text" class="form-control input-lg" id="lastName" required>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control input-lg" id="email" required>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="phone">Phone</label>
            <input type="phone" class="form-control input-lg" id="phone" required>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="product">Which RCB Product do you use?</label>
            <select id="product" class="form-control input-lg" required>
              <option value="checking">Checking</option>
              <option value="savings">Savings</option>
              <option value="Mortgage">Mortgage</option>
              <option value="CD's &amp; IRA's">CD's &amp; IRA's</option>
            </select>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="branch">Select a Branch</label>
            <select id="branch" class="form-control input-lg" required>
              <option value="Bartlesville">Bartlesville, OK</option>
              <option value="Blackwell">Blackwell, OK</option>
              <option value="Broken Arrow">Broken Arrow, OK</option>
              <option value="Catoosa">Catoosa, OK</option>
            </select>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <label for="branch">Enter your Message</label>
          <textarea class="form-control" rows="3"></textarea>
          <button type="submit" class="btn btn-brand-alt btn-lg mt-1">Send Message</button>
        </div>

      </div>

    </div>

  </form>

</div>




  <?php include('footer.php') ?>
