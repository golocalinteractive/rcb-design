<div id="search" class="modal" tabindex="-1" role="dialog">
  <button type="button" class="close hidden-xs" data-dismiss="modal" aria-label="Close"><span></span></button>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <!--<h3>What are you looking for?</h3>-->
            <form>
              <div class="input-group input-group-lg">
                <input type="text" name="search" class="form-control search-field" placeholder="Search RCB">
                <span class="input-group-btn">
                  <button class="btn btn-simple" type="button">Search</button>
                </span>
              </div><!-- /input-group -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->