<div id="contact" class="modal" tabindex="-1" role="dialog">
  <button type="button" class="close hidden-xs" data-dismiss="modal" aria-label="Close"><span></span></button>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <h4>Send us a message</h4>
            <form action="" class="contact-wrapper p-1 pb-1">

              <div class="container-fluid">

                <div class="row">

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="firstName">First Name</label>
                      <input type="text" class="form-control input-lg" id="firstName" required>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="lastName">Last Name</label>
                      <input type="text" class="form-control input-lg" id="lastName" required>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control input-lg" id="email" required>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="phone">Phone</label>
                      <input type="phone" class="form-control input-lg" id="phone" required>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="product">Which RCB Product do you use?</label>
                      <select id="product" class="form-control input-lg" required>
                        <option value="checking">Checking</option>
                        <option value="savings">Savings</option>
                        <option value="Mortgage">Mortgage</option>
                        <option value="CD's &amp; IRA's">CD's &amp; IRA's</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="branch">Select a Branch</label>
                      <select id="branch" class="form-control input-lg" required>
                        <option value="Bartlesville">Bartlesville, OK</option>
                        <option value="Blackwell">Blackwell, OK</option>
                        <option value="Broken Arrow">Broken Arrow, OK</option>
                        <option value="Catoosa">Catoosa, OK</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <label for="branch">Enter your Message</label>
                    <textarea class="form-control" rows="3"></textarea>
                    <button type="submit" class="btn btn-brand-alt btn-lg mt-1">Send Message</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->