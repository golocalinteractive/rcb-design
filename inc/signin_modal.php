<div id="signIn" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-content-inner">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title brand-primary-c">Welcome</h4>
        </div>
        <div class="modal-body">

          <form>
            <div class="form-group">
              <input type="text" class="form-control" id="userName" placeholder="Username">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" id="userName" placeholder="Password">
            </div>
            <div>
              <p><a href="/login-help">Need help logging in?</a></p>
            </div>
            <button class="btn btn-brand-alt-II btn-lg btn-block">Sign In</button>
          </form>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->