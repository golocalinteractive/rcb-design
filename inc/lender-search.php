	
<!-- Lender Search Component Stylesheet -->
<link href="css/lender-search.css" rel="stylesheet">

	<div class="locator">

		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"><h4 class="brand-primary-c fw-500 locator-title">Find a Lender</h4><h4 class="fw-500 search-by-lender">or <a href="location-finder-alt.php">Search by Location</a></h4></div>

				<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">

					<div class="finder-form">
						<div class="input-group find-a-branch-search">
							<div class="inner-addon left-addon">
								<input type="text" class="form-control" placeholder="Lender Name">
							</div>
							<span class="input-group-btn">
								<button class="btn brand-primary-mid brand-primary-white" type="button">Search</button>
							</span>
						</div><!-- /input-group -->
					</div>

				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 mt-xs-1">

					<div class="btn-group btn-block">
						<button class="btn btn-brand-alt btn-block btn-lg dropdown-toggle btn-location-filter" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Filter by <span class="glyphicon glyphicon-chevron-down"></span>
						</button>
						<ul class="loc-list-dropdown dropdown-menu pb-2">
							<div class="col-xs-12"><h5 class="fw-600 mt-1">Loan Specialist</h5></div>
							<div class="col-xs-12">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Mortage Specialists Only
									</label>
								</div>
							</div>
							<div class="col-xs-12"><h5 class="fw-600 mt-1">City</h5></div>
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Arkansas City
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Bartlesville
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Blackwell
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Broken Arrow
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Catoosa
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Claremore
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Collinsville
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Coweta
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Cushing
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Douglass
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Edmond
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Inola
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Lawrence
									</label>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Nichols Hills
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Norman
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Oilton
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Oklahoma City
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Owasso
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Ponca City
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Pryor
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Skiatook
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Stillwater
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Stroud
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Wellington
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Wichita
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Winfield
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" value="">Yukon
									</label>
								</div>
							</div>
						<div class="col-xs-12 col-sm-6 mt-1"><button class="btn btn-brand-alt btn-block">Apply filters</button></div>
						<div class="col-xs-12 col-sm-6 mt-1"><button class="btn btn-brand-alt btn-block">Clear filters</button></div>
						</ul>
					</div><!--/ locator -->

				</div>

			</div>

		</div>

	</div>