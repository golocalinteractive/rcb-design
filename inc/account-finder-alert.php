
<div class="modal account-finder-alert" id="accountFinderAlert" tabindex="-1" role="dialog" aria-labelledby="accountFinderAlert">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-content-inner">
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-12">
              <p class="brand-primary-c">Your answers strongly indicate you could be interested in a <a class="fw-600" href="product-detail.php">Money Market Account</a>, would you like to view this product's details now?</p>
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <a role="button" href="product-detail.php" class="btn btn-brand-alt btn-lg mt-1">View Details</a>
        <button type="button" class="btn btn-brand-mid btn-lg mt-1" data-dismiss="modal">Answer More Questions</button>
      </div>
      </div>
    </div>
  </div>
</div>