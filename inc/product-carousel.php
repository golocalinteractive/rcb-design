<div id="productCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#productCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#productCarousel" data-slide-to="1"></li>
    <li data-target="#productCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <div class="item active">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Cashback Checking</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Interest Checking</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Senior Checking</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Money Market</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-overdraft-protection"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Overdraft Protection</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Kids Checking</p></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!--/item-->

    <div class="item">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-personal-loans"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Personal Loans</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mortgage"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Mortgage</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mortgage-specialists"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Mortgage Specialists</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-home-equity"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Home Equity</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-overdraft-summer-loan"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Summer Loan</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-savings"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Savings</p></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!--/item-->

    <div class="item">
      <div class="container">
        <div class="row">
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-lg-offset-1">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-cd-ira"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">CD's &amp; IRA's</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-trust-investments"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Trust &amp; Investments</p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-cesa"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">CESA <span class="hidden-md">(Coverdell Education Savings Account)<span></p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">HSA <span class="hidden-md">(Health Savings Account)</span></p></a>
              </div>
            </div>
          </div>
          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-2">
            <div class="category-product mt-2 mb-2">
              <div class="category-product-icon">
                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>
              </div>
              <div class="category-product-body">
                <a href="/cashback-checking"><p class="brand-primary-mid-c">Kids Checking</p></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!--/item-->



  </div><!--/carousel-inner -->

  <!-- Controls -->
  <a class="left carousel-control hidden-xs" href="#productCarousel" role="button" data-slide="prev">
    <i class="fa fa-angle-left" aria-hidden="true"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control hidden-xs" href="#productCarousel" role="button" data-slide="next">
    <i class="fa fa-angle-right" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>

</div><!--/productCarousel -->