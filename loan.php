<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="../favicon.ico">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>RCB Bank | Loan Application</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

		<!-- font awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">

		<!-- RCB Stylesheet -->
		<link href="css/loan.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>

		<main>

			<div class="container">

				<div class="row">
					<div class="col-xs-12">
						<div class="top-strip">
							<div class="apply">apply</div>
							<div class="get-approved">get approved</div>
							<div class="movein">move in</div>
						</div>
					</div>
				</div>

			</div>

			<header class="page-header pt-3 pb-3 fs-1-1">

			  <div class="container">

			    <div class="row">

			      <div class="col-sm-12 col-md-12 col-lg-8">

			        <h1 class="fw-800 brand-primary-c">Apply Now for your perfect Loan. It's fast and easy!</h1>

			        <div class="row">
			        	<div class="col-lg-6"><p class="mt-1 fw-400 p-large">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit.</p></div>
						<div class="hidden-xs hidden-sm hidden-md col-lg-6"><img class="rarr" src="images/loan-images/rarr.png"></div>
					</div>

					<button data-scrollto=".aside-wrapper" class="btn btn-brand-alt btn-lg hidden-lg mt-1 scrollto-btn">Apply Now</button>

			      </div>

			    </div>

			  </div>

			</header>

			<div class="container">

		    	<div class="row relative">

		      		<div class="col-lg-8 pt-1">

		      			<article>

							<h3 class="brand-primary-c fw-600">Guarantee you get the loan that's right for you.</h3>
							<p>With today's constantly changing federal regulations, choosing a reputable, knowledagable and local lender can do more than just get you the lowest fees. Our Mortgage Specialists want to help make the daunting task of financing as smooth as possible for you.</p>
							<p class="brand-primary-c fw-800 pt-2">With an RCB loan you can*:</p>
							<div class="media">
							  <div class="media-left">
							  	<img width="30" src="images/loan-images/check.png">
							  </div>
							  <div class="media-body">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod incididunt.
							  </div>
							</div>
							<div class="media">
							  <div class="media-left">
							  	<img width="30" src="images/loan-images/check.png">
							  </div>
							  <div class="media-body">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod incididunt.
							  </div>
							</div>
							<div class="media">
							  <div class="media-left">
							  	<img width="30" src="images/loan-images/check.png">
							  </div>
							  <div class="media-body">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod incididunt.
							  </div>
							</div>
							<p class="mt-2">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
							<div class="logos">
								<div class="logo"><img src="images/loan-images/member_fdic.png"></div>
								<div class="logo"><img src="images/loan-images/equal_housing_lender.png"></div>
								<div class="logo"><img src="images/loan-images/bbb.png"></div>
								<div class="logo"><img src="images/loan-images/better_customer_service.png"></div>
							</div>
							<hr>
							<p class="small">* Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.</p>
							<p class="small"><a class="mr-2 brand-primary-c fw-600" href="">Terms &amp; Conditions</a><a class="brand-primary-c fw-600" href="">External link about government regulation</a></p>
					
						</article>

					</div>

					<div class="aside-wrapper">
						<aside>
							<h3 class="brand-primary-mid-c mb-1">Get Started Today</h3>
							<!-- form -->
							<form>
								<div class="form-group">
									<label for="productType">Product Type</label>
									<select class="form-control" required>
										<option>Purchase</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
								<div class="form-group">
									<label for="propertyState">Property State</label>
									<select class="form-control"required>
										<option>Purchase</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
								<div class="form-group">
									<label for="creditScore">Credit Score</label>
									<select class="form-control" required>
										<option>Purchase</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
								<label for="purchasePrice">Purchase Price</label>
								<div class="input-group mb-1">
								  <span class="input-group-addon">$</span>
								  <input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" required>
								  <span class="input-group-addon">.00</span>
								</div>
								<label for="downPayment">Down Payment</label>
								<div class="input-group mb-1">
								  <span class="input-group-addon">$</span>
								  <input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" required>
								  <span class="input-group-addon">.00</span>
								</div>
								<div class="form-group">
									<label for="firstName">First Name</label>
									<input type="text" class="form-control" id="firstName" required>
								</div>
								<div class="form-group">
									<label for="lastName">Last Name</label>
									<input type="text" class="form-control" id="lastName" required>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" class="form-control" id="email" required>
								</div>
								<div class="form-group">
									<label for="phoneNumber">Phone Number</label>
									<input type="tel" class="form-control" id="phoneNumber" required>
								</div>
							 	<button type="submit" class="btn btn-brand-alt btn-block btn-lg">Get Your Free Quote</button>
							</form>
						</aside>
						<p class="small mt-2 pl-1 pr-1">* At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
					</div>
				</div>
			</div>
		</main>

	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="js/bootstrap.min.js"></script>
	    <script>

		    function stickyNav(value, e) {

		      e.preventDefault();

		      // get the body top padding (if any) to substract from the scroll distance

		      var topPad = $('body').css('padding-top');
		      topPad = parseInt(topPad, 10);

		      if(value.length) {
		        $('html, body').animate({
		            scrollTop: $(value).offset().top - topPad
		        }, 300);
		      }
		    }

		    $(".scrollto-btn").on('click', function(e) {
		      var target = $(this).data("scrollto")
		      if(target) {
		        stickyNav(target, e);
		      }
		    });

	    </script>
	</body>
</html>