<?php include('header.php'); ?>

    <section class="container-fluid hero hero-compact hero-switch-kit">

      <div class="overlay"></div>

      <div class="row">

        <div class="col-lg-12">

          <div class="hero-content">

            <h5><div class="hero-icon"><span class="product-icon product-switch-kit"></span></div>Switch Kit</h5>

            <h1 class="brand-primary-white">Turn on the Rewards, Switch Today.</h1>

            <p>Switching isn't as hard as you may think. We're here to help you every step of the way.</p>

          </div>

        </div>

      </div>

    </section>


    <section>

    	<div class="container mt-2 mb-2">
    		<div class="row">
    			<div class="col-lg-12">
    				<h5 class="fw-600 mb-2">DOWNLOADABLE FORMS</h5>
    			</div>
    		</div>

    		<div class="row">

    			<div class="col-lg-4 col-md-6 col-sm-6 mb-xs-3">
    				<div class="brand-grey-very-light p-2 center-aligned brand-primary-border-btm-5">
    					<a href=""><img class="mb-2" src="images/oklahoma.png"></a>
    					<h3 class="fw-400">Oklahoma Residents</h3>
    					<p>Print and fill out this pdf to get started.</p>
    					<a role="button" class="btn btn-brand-alt btn-lg mt-2">Switch Kit <i class="ml-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
    				</div>
    			</div>

                <div class="col-lg-4 col-md-6 col-sm-6 mb-xs-3">
                    <div class="brand-grey-very-light p-2 center-aligned brand-primary-border-btm-5">
                        <a href=""><img class="mb-2" src="images/kansas.png"></a>
                        <h3 class="fw-400">Kansas Residents</h3>
                        <p>Print and fill out this pdf to get started.</p>
                        <a role="button" class="btn btn-brand-alt btn-lg mt-2">Switch Kit <i class="ml-2 fa fa-file-pdf-o" aria-hidden="true"></i></a>
                    </div>
                </div>

    			<div class="col-lg-4 col-md-12 col-sm-12 switch-kit-whats-inside">
    				<h4>What's Inside?</h4>
    				<ul>
    					<li>Account Switch Checklist</li>
    					<li>Direct deposit/Withdrawal Change Form</li>
    					<li>Account Closure Authorization Form</li>
    				</ul>
    			</div>
    		</div><!--/row-->
    	</div>
    </section>

    <div class="mt-4 pt-2 pb-2 switch-kit-wrapper">
    	<div class="container">

    		<div class="row">
    			<div class="col-lg-12">
    				<h5 class="fw-600 mb-2 mt-2">HOW IT WORKS</h5>
    			</div>
    		</div>
    		<div class="row flex">
                <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                    <img class="img-responsive width-100" src="images/switch-kit-one-sm.jpg">
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-push-6">
                    <ul class="list-unstyled switch-step">
                        <li><div class="number-icon"><span class="brand-primary-mid">1</span></div><h3>Open a New Account</h3></li>
                        <li><p>Fill out a <strong>Direct Deposit/Withdrawal Change Form</strong> and send it to all the companies you have automatic payments and direct deposits with.</p></li>
                        <li class="hidden-xs"><i class="fa fa-ellipsis-v fa-2x brand-primary-mid-c" aria-hidden="true"></i></li>
                    </ul>
                </div>
    			<div class="hidden-xs col-sm-6 col-sm-pull-6">
    				<img class="img-responsive width-100" src="images/switch-kit-one.jpg">
    			</div>
                <!-- start 2 -->
                <div class="col-xs-12 hidden-sm hidden-md hidden-lg mt-4">
                    <img class="img-responsive width-100" src="images/switch-kit-two-sm.jpg">
                </div>
                <div class="hidden-xs col-sm-6 col-sm-push-6">
                    <img class="img-responsive width-100" src="images/switch-kit-two.jpg">
                </div>
    			<div class="col-xs-12 col-sm-6 col-sm-pull-6">
    				<ul class="list-unstyled switch-step">
    					<li><div class="number-icon"><span class="brand-primary-mid">2</span></div></li>
    					<li><h3>Setup Online Banking</h3></li>
    					<li><p>Setup your RCB Bank online account and enter your online bill pay information. Our friendly staff can help you with this when you open your account.</p></li>
                        <li class="hidden-xs"><i class="fa fa-ellipsis-v fa-2x brand-primary-mid-c" aria-hidden="true"></i></li>
    				</ul>
    			</div>
                <!-- start 3 -->
                <div class="col-xs-12 hidden-sm hidden-md hidden-lg mt-4">
                    <img class="img-responsive width-100" src="images/switch-kit-three-sm.jpg">
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-push-6">
                    <ul class="list-unstyled switch-step">
                        <li><div class="number-icon"><span class="brand-primary-mid">3</span></div></li>
                        <li><h3>Discontinue Usage</h3></li>
                        <li><p>Stop using your old account and confirm all automatic transactions and outstanding checks have cleared the account.</p></li>
                        <li class="hidden-xs"><i class="fa fa-ellipsis-v fa-2x brand-primary-mid-c" aria-hidden="true"></i></li>
                    </ul>
                </div>
    			<div class="hidden-xs col-sm-6 col-sm-pull-6">
    				<img class="img-responsive width-100" src="images/switch-kit-three.jpg">
    			</div>
                <!-- start 4 -->
                <div class="col-xs-12 hidden-sm hidden-md hidden-lg mt-4">
                    <img class="img-responsive width-100" src="images/switch-kit-four-sm.jpg">
                </div>
                <div class="hidden-xs col-sm-6 col-sm-push-6 mb-2">
                    <img class="img-responsive width-100" src="images/switch-kit-four.jpg">
                </div>
    			<div class="col-xs-12 col-sm-6 col-sm-pull-6 mb-2">
    				<ul class="list-unstyled switch-step">
    					<li><div class="number-icon"><span class="brand-primary-mid">4</span></div></li>
    					<li><h3>Close Previous Account</h3></li>
    					<li><p>Fill out an <strong>Account Closure Authorization Form</strong> and send it to your old bank. You might have to stop by your old bank and fill out their account closure form. Call ahead to verify.</p></li>
                    </ul>
    			</div>
    		</div>
            <div class="row">
                <div class="col-lg-12 center-aligned">
                   <h3 class="mt-1 fw-500">Start Your <span class="brand-primary-mid-c">Switch Today</span></h3>
                   <a role="button" class="btn btn-brand-alt btn-lg mt-1" href="/">Find a Branch</a>
                </div>
            </div>

    	</div>

    </div>

    <section class="callout mt-2 mb-2">

		<div class="container-fluid">

	        <div class="row">

	 			<div class="col-lg-12">

		            <div class="callout-body p-2 brand-primary">

		              <h3 class="brand-primary-light-c fw-800 mb-1 uppercase">Switch Kit</h3>

		              <h2 class="brand-primary-white mb-03"><b>What You Need to Know</b>

		              <h3 class="fw-500 muddymint-green mb-1">Some companies have special requirements for closing an account. Learn more about how to switch.</h3>

		              <a href="/personal-loans" role="button" class="btn btn-brand-on-green btn-lg">Get Started</a>

		            </div>

				</div>

	        </div>

		</div>

    </section>



<?php include('footer.php'); ?>