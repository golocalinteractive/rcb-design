<?php include('header.php'); ?>

    <section class="container-fluid hero hero-personal-checking">

      <div class="overlay"></div>

      <div class="row">

        <div class="col-lg-12">

          <div class="hero-content">

            <h5>Personal Checking</h5>

            <h1 class="brand-primary-white">RCB Bank Rewards You.</h1>

            <p>Earn cash back while you use our personal checking options and grow your wealth with RCB Bank. It's our way of saying thank you.</p>

            <button data-scrollto=".categories-hdr" class="btn btn-brand-alt btn-lg mt-1 scrollto-btn">View Products</button>

          </div>

        </div>

      </div>

    </section>

    <header class="page-header categories-hdr">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-grey-c">Check Out Our <span class="brand-primary-mid-c">Rewards Checking Options</span></h2>

          </div>

        </div>

      </div>

    </header>

    <section class="categories">

      <div class="container-fluid">

        <div class="row flex">

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-icon">

                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>

              </div>

              <div class="category-product-body">

                <a href="product-detail.php"><h4 class="brand-primary-mid-c">Cashback Checking</h4></a>

                <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

                <a href="product-detail.php" role="button" class="btn btn-brand-alt mt-1">Learn More</a>

              </div>

            </div>

          </div><!--/col -->


          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-icon">

                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>

              </div>

              <div class="category-product-body">

                <a href="/interest-checking"><h4 class="brand-primary-mid-c">Interest Checking</h4></a>

                <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

                <a href="/cashback-checking" role="button" class="btn btn-brand-alt mt-1">Learn More</a>

              </div>

            </div>

          </div><!--/col -->


          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-icon">

                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>

              </div>

              <div class="category-product-body">

                <a href="/senior-checking"><h4 class="brand-primary-mid-c">Senior Checking</h4></a>

                <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

                <a href="/cashback-checking" role="button" class="btn btn-brand-alt mt-1">Learn More</a>

              </div>

            </div>

          </div><!--/col -->


          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-icon">

                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span></a>

              </div>

              <div class="category-product-body">

                <a href="/money-market"><h4 class="brand-primary-mid-c">Money Market</h4></a>

                <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

                <a href="/cashback-checking" role="button" class="btn btn-brand-alt mt-1">Learn More</a>

              </div>

            </div>

          </div><!--/col -->


          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-icon">

                <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-kids"></span></a>

              </div>

              <div class="category-product-body">

                <a href="/kids-checking"><h4 class="brand-primary-mid-c">Kids Checking</h4></a>

                <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

                <a href="/cashback-checking" role="button" class="btn btn-brand-alt mt-1">Learn More</a>

              </div>

            </div>

          </div><!--/col -->

        </div>

      </div>

    </section>

    <header class="page-header mt-2">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-grey-c">Find a Checking Option <span class="brand-primary-mid-c">Right for You</span></h2>

          </div>

        </div>

      <div>

    </header>

    </section><!-- / product table -->

    <section class="callout mt-2 mb-2">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <div class="callout-body p-2 brand-primary">

              <h3 class="brand-primary-light-c fw-800 mb-1 uppercase">Account Finder</h3>

              <h2 class="brand-primary-white mb-03"><b>Which program is right for you?</b>

              <h3 class="fw-500 muddymint-green mb-1">Answer a few short questions and we'll highlight a program that best fits your needs.</h3>

              <a href="/personal-loans" role="button" class="btn btn-brand-on-green btn-lg">Get Started</a>

            </div>

          </div>

        </div>

      </div>

    </section>

    <section class="product-table mt-2 mb-1">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-2 hidden-md hidden-sm hidden-xs">

            <ul class="list-unstyled">
              <li class="left-aligned ml-1 product-table-label">Features</li>
              <li class="left-aligned ml-1">Minimum to open</li>
              <li class="left-aligned ml-1 stripe">Service Charge</li>
              <li class="left-aligned ml-1">Charge Reward</li>
              <li class="left-aligned ml-1 stripe">Qualifications</li>
              <li class="left-aligned ml-1">Check Card</li>
              <li class="left-aligned ml-1 stripe">Transactions</li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Cashback</b><span class="grey-light-c">Pays you back, with Cash</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>Cash Back</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/cashback-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-mid brand-primary-white uppercase product-table-label"><b>Interest</b><span class="grey-light-c">Pays you back, with Interest</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>Interest</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/interest-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Senior</b><span class="grey-light-c">62 and Better</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>Interest</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/senior-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-mid brand-primary-white uppercase product-table-label"><b>Money Market</b><span class="grey-light-c">High yield money market account</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>$10 mo &lt; $2,500</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Transactions</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/money-market" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Kids</b><span class="grey-light-c">18 and under</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$20</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>NA</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>No</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/kids-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

        </div>

      </div>

    </section>

    <header class="page-header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-grey-c">RCB Bank <span class="brand-primary-mid-c">Learning Center</span></h2>

          </div>

        </div>

      </div>

    </header>

    <section class="learning-center mt-1 mb-1">

      <div class="container-fluid">

        <div class="row flex mt-1">

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

        </div><!-- /row -->

        <div class="row">

          <div class="col-lg-12">

            <h4><a class="brand-primary-mid-c" href="/learing-center">Visit the Learning Center &raquo;</a></h4>

          </div>

        </div>

      </div>

    </section>


  <?php include('footer.php') ?>
