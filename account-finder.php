<?php include('header.php'); ?>

<!-- RCB Blog Stylesheet -->
<link href="css/account-finder.css" rel="stylesheet">

<section class="container-fluid hero brand-gradient-animated">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

        <h1 class="brand-primary-white">Account Finder</h1>

        <p>Use our online tool to find a product that best fits your banking needs.</p>

		<button data-scrollto=".account-finder" class="btn btn-brand-on-light-green btn-lg scrollto-btn">Get Started</button>

      </div>

    </div>

  </div>

</section>

<section class="account-finder">

	<div class="container-fluid">

		<div class="row">

			<div class="col-lg-6">

				<div class="account-finder-wrapper mt-1 mb-1 pt-2 pb-2">

					<p>Question 2 of 8</p>

					<div class="progress-bar"><span style="width:12.5%;margin-left:12.5%;"></span></div>

					<p><strong>2. Choose which thing you have a preference for:</strong></p>

					<ul class="list-unstyled">

						<li>
							<div class="radio">
							  <label>
							    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							    I prefer option one.
							  </label>
							</div>
						</li>

						<li>
							<div class="radio">
							  <label>
							    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
							    Option two is the best choice. <button type="button" class="account-finder-tooltip" data-toggle="tooltip" data-placement="top" data-container="body" title="a tooltip pops up when they hover with a short explanation or related tip so they can  get a better understanding of the option."><i class="fa fa-question-circle" aria-hidden="true"></i></button>
							  </label>
							</div>
						</li>

						<li>
							<div class="radio">
							  <label>
							    <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
							    I really like this option the most.
							  </label>
							</div>
						</li>
						<li>
							<div class="radio">
							  <label>
							    <input type="radio" name="optionsRadios" id="optionsRadios4" value="option4">
							    Hmm, this seems the most logical choice.
							  </label>
							</div>
						</li>

						<li>
							<div class="radio">
							  <label>
							    <input type="radio" name="optionsRadios" id="optionsRadios5" value="option5">
							    I'm sure this is the right answer.
							  </label>
							</div>
						</li>

						<li>
							<a role="button" class="btn btn-brand-alt btn-lg mt-1" data-toggle="modal" data-target="#accountFinderAlert">Next Question &nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</li>

					</ul>




					<p class="mt-4">Question 3 of 8</p>

					<div class="progress-bar"><span style="width:12.5%;margin-left:25%;"></span></div>

					<p><strong>3. Arrange these items in order of most to least important:</strong></p>

					<ul id="sortable" class="list-unstyled sortable">

						<li class="ui-state-default"><div class="handle"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div><div>I prefer option one.</div></li>

						<li class="ui-state-default"><div class="handle"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div><div>I think option two is the best choice.</div></li>

						<li class="ui-state-default"><div class="handle"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div><div>I really like this option the most.</div></li>
						
						<li class="ui-state-default"><div class="handle"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div><div>Hmm, this seems the most logical choice.</div></li>

						<li class="ui-state-default"><div class="handle"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div><div>I'm sure this is the right answer.</div></li>

					</ul>

					<a role="button" class="btn btn-brand-alt btn-lg mt-1" data-toggle="modal" data-target="#accountFinderAlert">Next Question &nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

	</div>

</section>

<header class="page-header categories-hdr">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h2 class="fw-500 brand-grey-c"><span class="brand-primary-mid-c">Congratulations!</span></h2>
        <p class="mt-1">Based on your answers, we think you'll be really happy with a <b>Money Market</b> Account.</p>

      </div>

    </div>

  </div>

</header>

<section class="product-table account-finder-table mt-4 mb-1">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-2 hidden-md hidden-sm hidden-xs">

	    <ul class="list-unstyled">
	      <li class="left-aligned ml-1 product-table-label">Features</li>
	      <li class="left-aligned ml-1">Minimum to open</li>
	      <li class="left-aligned ml-1 stripe">Service Charge</li>
	      <li class="left-aligned ml-1">Charge Reward</li>
	      <li class="left-aligned ml-1 stripe">Qualifications</li>
	      <li class="left-aligned ml-1">Check Card</li>
	      <li class="left-aligned ml-1 stripe">Transactions</li>
	    </ul>

      </div>

      <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

      	<a href="product-detail.php">

	        <ul class="list-unstyled account-selected">
	          <li class="brand-primary-mid brand-primary-white uppercase product-table-label"><b>Money Market</b><span>High yield money market account</span></li>
	          <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
	          <li><div class="small-device-label hidden-lg">Service Charge</div>None</li>
	          <li><div class="small-device-label hidden-lg">Charge Reward</div>$10 mo &lt; $2,500</li>
	          <li><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
	          <li><div class="small-device-label hidden-lg">Transactions</div>Yes</li>
	          <li><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
	          <li><div class="btn btn-brand-alt">Learn More</div></li>
	        </ul>

	    </a>

      </div>

      <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

      	<a href="product-detail.php">

	        <ul class="list-unstyled">
	          <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Cashback</b><span class="grey-light-c">Pays you back, with Cash</span></li>
	          <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
	          <li><div class="small-device-label hidden-lg">Charge Reward</div>Cash Back</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
	          <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
	          <li><div class="btn btn-brand-alt">Learn More</div></li>
	        </ul>

	    </a>

      </div>

      <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

      	<a href="product-detail.php">

	        <ul class="list-unstyled">
	          <li class="grey-mid brand-primary-white uppercase product-table-label"><b>Interest</b><span class="grey-light-c">Pays you back, with Interest</span></li>
	          <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
	          <li><div class="small-device-label hidden-lg">Charge Reward</div>Interest</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
	          <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
	          <li><div class="btn btn-brand-alt">Learn More</div></li>
	        </ul>

	    </a>

      </div>

      <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

      	<a href="product-detail.php">

	        <ul class="list-unstyled">
	          <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Senior</b><span class="grey-light-c">62 and Better</span></li>
	          <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
	          <li><div class="small-device-label hidden-lg">Charge Reward</div>Interest</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
	          <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
	          <li><div class="btn btn-brand-alt">Learn More</div></li>
	        </ul>

	    </a>

      </div>

      <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

      	<a href="product-detail.php">

	        <ul class="list-unstyled">
	          <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Kids</b><span class="grey-light-c">18 and under</span></li>
	          <li><div class="small-device-label hidden-lg">Minimum to open</div>$20</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
	          <li><div class="small-device-label hidden-lg">Charge Reward</div>NA</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
	          <li><div class="small-device-label hidden-lg">Check Card</div>No</li>
	          <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
	          <li><div class="btn btn-brand-alt">Learn More</div></li>
	        </ul>

	    </a>

      </div>

    </div>

  </div>

</section><!-- / product table -->

<section class="af-answers mt-2"><!-- account finder answers -->

	<div class="container-fluid">

		<div class="row">

			<div class="col-lg-12">

				<a role="button" data-toggle="collapse" href="#answers" aria-expanded="false" aria-controls="Account Finder Answers">
				<span class="fw-600">Show Your Answers &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></span>
				</a>

				<a class="ml-3" href="account-finder.php"><span class="fw-600">Start Over</span></a>

				<div class="collapse" id="answers">
					<ol>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question one.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question two.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question three.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question four.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question five.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question six.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question seven.</li>
						<li><span class="brand-primary-mid-c">Choose which thing you have a preference for?</span><br />Answer to question eight.</li>
					</ol>
				</div>

			</div>

	</div>

</section><!-- / account finder answers -->

<?php include('inc/account-finder-alert.php') ?>

<?php include('footer.php') ?>

<!-- jQuery UI for draggable re-ordering of list items. Includes Core: all, Interactions: all,  Widgets: Mouse, Effects: All (but all are not necessary for this feature) https://jqueryui.com/draggable/#sortable -->

<script src="js/jquery-ui.min.js"></script>

<script src="js/jquery.ui.touch-punch.min.js"></script>

<script>
  $( function() {
    $( "#sortable" ).sortable({
      handle:".handle",
      revert: true
    });
    $( "#draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
  } );
</script>
