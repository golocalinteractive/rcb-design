</main>

<footer class="mt-2 pt-1 pl-1 pr-1 pb-1">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="brand-primary-c fw-500 footer-accordion-btn pb-1 mb-1"><div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></div>All RCB Bank Products</h4>
            </div>
        </div>

        <div id="footerProducts" class="row flex hidden">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="brand-primary-c fw-600 uppercase">Checking</h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Cashback</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Interest</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quanderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Senior</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Money Market</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulnderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Business Analysis</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Business Checking</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia quptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Non Profit</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consecerit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Business Interest</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="brand-primary-c fw-600 uppercase">Savings</h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">CESA</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia m reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">HSA</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. D eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Kids</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Business Leasing</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliqoluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="brand-primary-c fw-600 uppercase">Loans</h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Summer Loan</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur qi sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Mortgage</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Consumer Loans</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequat quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Home Equity</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla atem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="brand-primary-c fw-600 uppercase">Investments</h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">CD's &amp; IRA's</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaeraenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Trust Services</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed almollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Wealth Management</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Home Equity</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. </p>
                  </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="brand-primary-c fw-600 uppercase">Anytime Banker</h5>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Bank App</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nuleprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Bank by Phone</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid . Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Text Banking</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas clitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Online Banking</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="ccol-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                <div class="category-product mt-1 mb-1">
                  <div class="category-product-body">
                    <a href="/cashback-checking"><h5 class="brand-primary-mid-c">Mobile Deposit</h5></a>
                    <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>
                  </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="brand-primary-c fw-600 uppercase">More Services</h5>
            </div>
            <div class="col-lg-12">
                <ul class="list-unstyled footer-inline-list mb-2">
                    <li><a href="">Reorder Checks</a></li>
                    <li><a href="">EMV Chip Card</a></li>
                    <li><a href="">Lost/Stolen Card</a></li>
                    <li><a href="">Lock Box</a></li>
                    <li><a href="">Remote Desktop Capture</a></li>
                    <li><a href="">Merchant Card Services</a></li>
                    <li><a href="">Account View</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
        </div>

        <div class="row">
            <div class="footer-address-wrapper col-sm-3 col-xs-12">
                <ul class="list-unstyled brand-primary-c">
                    <li><h5 class="mt-0 mb-0 fw-600">&copy; 2017 RCB Bank</h5></li>
                    <li>P.O. Box 189</li>
                    <li>74018</li>
                    <li><a class="brand-primary-mid-c" href="tel:918-341-6150">918.341.6150</a></li>
                    <li>NMLS #798151</li>
                    <li><img width="50" src="images/memberFDIC.png"> &nbsp; &nbsp; <img src="images/ehl.png"></li>
                </ul>
            </div>
            <div class="footer-loc-wrapper col-sm-9 col-xs-12">
                <ul class="list-unstyled footer-inline-list mb-2">
                    <li class="title"><h5 class="brand-primary-c fw-600 mt-0">ATM/Branch Locations</h5></li>
                    <li><a href="">Bartlesville</a></li>
                    <li><a href="">Blackwell</a></li>
                    <li><a href="">Broken Arrow</a></li>
                    <li><a href="">Catoosa</a></li>
                    <li><a href="">Claremore</a></li>
                    <li><a href="">Collinsville</a></li>
                    <li><a href="">Coweta</a></li>
                    <li><a href="">Cushing</a></li>
                    <li><a href="">Drumright</a></li>
                    <li><a href="">Edmond</a></li>
                    <li><a href="">Inola</a></li>
                    <li><a href="">Nichols Hills</a></li>
                    <li><a href="">Norman</a></li>
                    <li><a href="">Oilton</a></li>
                    <li><a href="">Oklahoma City</a></li>
                    <li><a href="">Owasso</a></li>
                    <li><a href="">Ponca City</a></li>
                    <li><a href="">Pryor</a></li>
                    <li><a href="">Shidler</a></li>
                    <li><a href="">Skiatook</a></li>
                    <li><a href="">Stillwater</a></li>
                    <li><a href="">Stroud</a></li>
                    <li><a href="">Yukon*</a></li>
                    <li><a href="">Arkansas City</a></li>
                    <li><a href="">Douglass</a></li>
                    <li><a href="">Lawrence</a></li>
                    <li><a href="">Oxford</a></li>
                    <li><a href="">Wellington</a></li>
                    <li><a href="">Wichita</a></li>
                    <li><a href="">Winfield</a></li>
                </ul>
            </div>
            <div class="footer-nav-wrapper col-sm-3 col-xs-6">
                <ul class="list-unstyled brand-primary-c">
                    <li><a href="/personal">Personal</a></li>
                    <li><a href="/business">Business</a></li>
                    <li><a href="/personal">Why RCB Bank?</a></li>
                    <li><a href="/personal">Find an ATM/Branch</a></li>
                    <li><a href="/personal">Learning Center</a></li>
                    <li><a href="/personal">News</a></li>
                </ul>
            </div>
            <div class="footer-nav-wrapper col-sm-3 col-xs-6">
                <ul class="list-unstyled brand-primary-c">
                    <li><a href="/personal">FDIC Deposit Insurance</a></li>
                    <li><a href="/personal">Personal Online Security</a></li>
                    <li><a href="/personal">Privacy Policy</a></li>
                    <li><a href="/personal">Privacy Notice</a></li>
                    <li><a href="/personal">Sign In/Enroll</a></li>
                    <li><a href="/personal">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <p class="brand-grey-c">FDIC Insurance Coverage Change: By federal law, as of 1/1/2014, funds in a noninterest-bearing transaction account (including IOLTA/IOLA) will no longer receive unlimited deposit insurance coverage, but will be FDIC-insured to the legal maximum of $250,000 for each ownership category. <a class="brand-primary-mid-c" href="/fdic_deposit_insurance">Learn More.</a></p>
            </div>
        </div>
    </div>
</footer>


    <?php include('inc/signin_modal.php'); ?>
    <?php include('inc/search_modal.php'); ?>
    <?php include('inc/contact_modal.php'); ?>
    <!-- nav scripts -->
    <script src="js/modernizr.custom.js"></script>
    <script src="js/cbpHorizontalSlideOutMenu.min.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Adds touch support to carousels via bcSwipe.js -->
    <script src="js/jquery.bcSwipe.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- custom js -->
    <script src="js/functions.js"></script>

  </body>
</html>