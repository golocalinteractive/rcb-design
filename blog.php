<?php include('header.php'); ?>

<!-- RCB Blog Stylesheet -->
<link href="css/blog.css" rel="stylesheet">

<div class="container-fluid">

	<div class="row hidden-lg hidden-md blog-tabs-wrapper" data-spy="affix" data-offset-top="77">

		<div>
			<!-- Nav tabs -->
			<ul class="nav blog-tabs nav-tabs" role="tablist">
				<li class="read-tab-tab" role="presentation" class="active"><a href="#read" aria-controls="read" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-sunglasses"></span> Read</a></li>
				<li class="browse-tab-tab" role="presentation"><a href="#browse" aria-controls="browse" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Browse</a></li>
	  		</ul>
	  	</div>

	</div>

	<div class="blog-tabs-placeholder"></div>

	<div class="tab-content row blog-content">

		<div role="tabpanel" id="browse" class="tab-pane">

			<aside class="sidenav">

				<div class="trending">

					<ul class="list-unstyled trending-content">
						<li>
							<ol class="breadcrumb">
							  <li><a href="#">Learning Center</a></li>
							  <li class="active">Financial Basics</li>
							</ol>
						</li>
						<li class="active">
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
							<hr>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									How to Gift Shop and Stay Debt Free This Holiday Season
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									How to Gift Shop and Stay Debt Free This Holiday Season
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									How to Gift Shop and Stay Debt Free This Holiday Season
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									How to Gift Shop and Stay Debt Free This Holiday Season
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-left media-middle">
								      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
								  </div>
								  <div class="media-body">
									The Secret Sauce to Achieving Your Goals
								  </div>
								</div>
							</a>
						</li>
						<li>
							<a href="">
								<div class="media">
								  <div class="media-body">
									<button class="btn btn-brand-alt btn-block">Load More</button>
								  </div>
								</div>
							</a>
						</li>
					</ul>

				</div>

			</aside>

		</div>

		<div role="tabpanel" id="read" class="article tab-pane active">

			<article>

				<h1 class="brand-primary-c fw-600">The Secret Sauce to Achieving Your Goals</h1>

				<p class="lead">What if I told you goals are like potatoes? Let me explain.</p>

				<div class="media byline mb-2">
				  <div class="media-left media-middle">
				      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
				  </div>
				  <div class="media-body">
					<small><span class="fw-600">Kyle Head</span><br />Product Coordinator</small>
				  </div>
				</div>

				<div class="row article-social clearfix">
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-pdf mt-xs-1 mb-xs-1"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-pdf mt-xs-1 mb-xs-1"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-facebook mt-xs-1 mb-xs-1"><i class="fa fa-facebook-square" aria-hidden="true"></i></button></div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-twitter mt-xs-1 mb-xs-1"><i class="fa fa-twitter" aria-hidden="true"></i></button></div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-googleplus mt-xs-1 mb-xs-1"><i class="fa fa-google-plus" aria-hidden="true"></i></button></div>					
				</div>

				<img class="img-responsive mb-2" src="images/why-hero-md.jpg">

				<p>One type of goal is like instant potatoes. Stir in water, heat up and enjoy. The other type is like real mashed potatoes. It's a process that takes time, planning and work. First you have to peel the potatoes, then you cook them, and then you have to mash them until you get rid of all the lumps.</p>
				<p>The goals we set in life aren’t always going to be instant, but with planning and effort we can enjoy savory results.</p>
				<p>A University of Scranton research study suggests an estimated 45 percent of people make a New Year’s resolution. Of those, only 8 percent actually achieve their stated goal.</p>
				<p>Why? Could it be how we’re setting our goals? Let’s look at a few ways we set goals and find the secret ingredient to achieving them.</p>
				<h3>Friend Zone</h3>
				<p>You tell your friend Steve your goal is to save up money to take a vacation this summer. You ask him to help you stay on track by not letting you eat out at lunch. Is Steve the friend who will hold you accountable? Or could he forget and fail to stop you in a moment of weakness?</p>
				<h3>Social Media Black Hole</h3>
				<p>You post your vacation goal on social media for family, friends and even people you don’t really know to see so they can support you along your journey. But what happens when other posts crowd your friends’ pages? Will your supporters fall to the wayside, never to be seen again?</p>
				<hr>
				<h4 class="black-c">Related Articles</h4>
				<div class="row related-articles">
					<div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-4 mt-1">
			            <div class="card">
			              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
			              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
			            </div>
					</div>
					<div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-4 mt-1">
			            <div class="card">
			              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
			              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
			            </div>
					</div>
					<div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-4 mt-1">
			            <div class="card">
			              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
			              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
			            </div>
					</div>
				</div>
			</article>

			<article>

				<h1 class="brand-primary-c fw-600">How to Gift Shop and Stay Debt Free This Holiday Season</h1>

				<div class="media byline mb-2">
				  <div class="media-left media-middle">
				      <img class="media-object" src="images/placeholder.jpg" alt="placeholder">
				  </div>
				  <div class="media-body">
					<small><span class="fw-600">Kyle Head</span><br />Product Coordinator</small>
				  </div>
				</div>

				<div class="row article-social clearfix">
					<div class="col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-pdf mt-xs-1 mb-xs-1"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></div>
					<div class="col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-facebook mt-xs-1 mb-xs-1"><i class="fa fa-facebook-square" aria-hidden="true"></i></button></div>
					<div class="col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-twitter mt-xs-1 mb-xs-1"><i class="fa fa-twitter" aria-hidden="true"></i></button></div>
					<div class="col-sm-3 col-md-3 col-lg-3"><button class="btn btn-block btn-share-googleplus mt-xs-1 mb-xs-1"><i class="fa fa-google-plus" aria-hidden="true"></i></button></div>					
				</div>

				<img class="img-responsive mb-2" src="images/why-hero-md.jpg">

				<p>One type of goal is like instant potatoes. Stir in water, heat up and enjoy. The other type is like real mashed potatoes. It's a process that takes time, planning and work. First you have to peel the potatoes, then you cook them, and then you have to mash them until you get rid of all the lumps.</p>
				<p>The goals we set in life aren’t always going to be instant, but with planning and effort we can enjoy savory results.</p>
				<p>A University of Scranton research study suggests an estimated 45 percent of people make a New Year’s resolution. Of those, only 8 percent actually achieve their stated goal.</p>
				<p>Why? Could it be how we’re setting our goals? Let’s look at a few ways we set goals and find the secret ingredient to achieving them.</p>
				<h3>Friend Zone</h3>
				<p>You tell your friend Steve your goal is to save up money to take a vacation this summer. You ask him to help you stay on track by not letting you eat out at lunch. Is Steve the friend who will hold you accountable? Or could he forget and fail to stop you in a moment of weakness?</p>
				<h3>Social Media Black Hole</h3>
				<p>You post your vacation goal on social media for family, friends and even people you don’t really know to see so they can support you along your journey. But what happens when other posts crowd your friends’ pages? Will your supporters fall to the wayside, never to be seen again?</p>
				<hr>
				<h4 class="black-c">Related Articles</h4>
				<div class="row related-articles">
					<div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-4 mt-1">
			            <div class="card">
			              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
			              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
			            </div>
					</div>
					<div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-4 mt-1">
			            <div class="card">
			              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
			              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
			            </div>
					</div>
					<div class="col-xxs-12 col-xs-6 col-sm-4 col-md-4 col-lg-4 mt-1">
			            <div class="card">
			              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
			              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
			            </div>
					</div>
				</div>
			</article>

		</div>

	</div>

</div>

<?php include('footer.php') ?>
