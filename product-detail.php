<?php include('header.php'); ?>

    <section class="container-fluid hero hero-compact hero-cashback-checking">

      <div class="overlay"></div>

      <div class="row">

        <div class="col-lg-12">

          <div class="hero-content hero-content-compact">

            <h5><div class="hero-icon"><span class="product-icon product-icon-checking"></span></div>Cashback Checking</h5>

            <h1 class="brand-primary-white">Earn Cash Back With RCB Bank.</h1>

            <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Not sure which product is right for you? Try our <a class="brand-primary-light-c" href="account-finder.php?=personal-checking">Account Finder</a>.</p>

            <a role="button" href="location-finder.php" class="btn btn-brand-on-light-green btn-lg mt-1 scrollto-btn">Find a Branch</a>
            <a role="button" href="account-finder.php?=personal-checking" class="btn btn-brand-on-light-green btn-lg mt-1">Account Finder</a>

          </div>

        </div>

      </div>

    </section>

    <header class="page-header page-header-alt categories-hdr">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-primary-white">The Checking Account that <span class="brand-primary-mid-c">Pays You Back.</span></h2>

          </div>

        </div>

      </div>

    </header>


    <section class="mt-2 mb-1">

      <div class="container-fluid">

        <div class="row flex">

          <div class="col-xs-12 col-sm-6 col-md-10 col-lg-5">

            <div class="category-product left-green-border-wrapper mt-2 mb-2">

              <div class="category-product-body">

                <p class="lead mb-0">Earn cash back on all debit card transactions excluding ATM transactions and refunds. No monthly service charge. No minimum balance.</p>

              </div>

            </div>

          </div><!--/col -->

          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-3">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-body">

                <h4 class="fw-500 brand-primary-mid-c mt-0">Requirements</h4>
                  <ul class="list-styled">
                  <li>Use your debit card for purchases at least 10 times per rewards cycle</li>
                  <li>Receive e-statments</li>
                </ul>

              </div>

            </div>

          </div><!--/col -->

          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-3">

            <div class="category-product mt-2 mb-2">

              <div class="category-product-body">

                <h4 class="fw-500 brand-primary-mid-c mt-0">Rewards</h4>
                <ul class="list-styled">
                  <li>10 cents on every purchase $10+</li>
                  <li>5 cents on every purchase under $10</li>
                </ul>

              </div>

            </div>

          </div><!--/col -->

          <div class="col-lg-12">

            <h4><a class="brand-primary-mid-c" href="/find-a-branch">Find a Branch Near You &raquo;</a></h4>

          </div>

      </div>

    </section>

    <header>

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12 center-aligned">

            <h3 class="fw-500 brand-grey-c">View Related products</h3>

          </div>

        </div>

      <div>

    </header>

    <section>

        <?php include('inc/product-carousel.php'); ?>

    </section>

    <section class="callout mt-2 mb-2">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <div class="callout-body p-2 brand-primary">

              <h3 class="brand-primary-light-c fw-800 mb-1 uppercase">Account Finder</h3>

              <h2 class="brand-primary-white mb-03"><b>Which program is right for you?</b>

              <h3 class="fw-500 muddymint-green mb-1">Answer a few short questions and we'll highlight a program that best fits your needs.</h3>

              <a href="/personal-loans" role="button" class="btn btn-brand-on-green btn-lg">Get Started</a>

            </div>

          </div>

        </div>

      </div>

    </section>

    <header class="page-header mt-2">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-grey-c">Find a Checking Option <span class="brand-primary-mid-c">Right for You</span></h2>

          </div>

        </div>

      <div>

    </header>

    <section class="product-table mt-2 mb-1">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-2 hidden-md hidden-sm hidden-xs">

            <ul class="list-unstyled">
              <li class="left-aligned ml-1 product-table-label">Features</li>
              <li class="left-aligned ml-1">Minimum to open</li>
              <li class="left-aligned ml-1 stripe">Service Charge</li>
              <li class="left-aligned ml-1">Charge Reward</li>
              <li class="left-aligned ml-1 stripe">Qualifications</li>
              <li class="left-aligned ml-1">Check Card</li>
              <li class="left-aligned ml-1 stripe">Transactions</li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Cashback</b><span class="grey-light-c">Pays you back, with Cash</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>Cash Back</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/cashback-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-mid brand-primary-white uppercase product-table-label"><b>Interest</b><span class="grey-light-c">Pays you back, with Interest</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>Interest</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/interest-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Senior</b><span class="grey-light-c">62 and Better</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>Interest</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/senior-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-mid brand-primary-white uppercase product-table-label"><b>Money Market</b><span class="grey-light-c">High yield money market account</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$100</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>$10 mo &lt; $2,500</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Transactions</div>Yes</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/money-market" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

          <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">

            <ul class="list-unstyled">
              <li class="grey-dark brand-primary-white uppercase product-table-label"><b>Kids</b><span class="grey-light-c">18 and under</span></li>
              <li><div class="small-device-label hidden-lg">Minimum to open</div>$20</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Service Charge</div>None</li>
              <li><div class="small-device-label hidden-lg">Charge Reward</div>NA</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Qualifications</div>Yes</li>
              <li><div class="small-device-label hidden-lg">Check Card</div>No</li>
              <li class="stripe"><div class="small-device-label hidden-lg">Transactions</div>Unlimited</li>
              <li><a href="/kids-checking" role="button" class="btn btn-brand-alt">Learn More</a></li>
            </ul>

          </div>

        </div>

      </div>

    </section><!-- / product table -->

    <header class="page-header">

      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-12">

            <h2 class="fw-500 brand-grey-c">RCB Bank <span class="brand-primary-mid-c">Learning Center</span></h2>

          </div>

        </div>

      </div>

    </header>

    <section class="learning-center mt-1 mb-1">

      <div class="container-fluid">

        <div class="row flex mt-1">

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
            <div class="card">
              <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
              <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
            </div>
          </div>

        </div><!-- /row -->

        <div class="row">

          <div class="col-lg-12">

            <h4><a class="brand-primary-mid-c" href="/learing-center">Visit the Learning Center &raquo;</a></h4>

          </div>

        </div>

      </div>

    </section>


  <?php include('footer.php') ?>
