<?php include('header.php'); ?>

<section class="container-fluid hero hero-personal-checking">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

        <h5>Leadership</h5>

        <h1 class="brand-primary-white">Customer Service is Our Mission.</h1>

        <p>We are ethical, trustworthy and sincere. We know our customers by name and they know us by ours!</p>

      </div>

    </div>

  </div>

</section>

<header class="page-header categories-hdr">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h2 class="fw-500">We Are Business Leaders <span class="brand-primary-mid-c">Dedicated to Your Community.</span></h2>

      </div>

    </div>

  </div>

</header>

<section class="staff-wrapper m-0 pl-1 pr-1 pb-2">

	<div class="container-fluid">

		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
				<div id="staffOne" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffOne" data-slide-to="0" class="active"></li>
						<li data-target="#staffOne" data-slide-to="1"></li>
						<li data-target="#staffOne" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffOne" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffOne" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
				<div id="staffTwo" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffTwo" data-slide-to="0" class="active"></li>
						<li data-target="#staffTwo" data-slide-to="1"></li>
						<li data-target="#staffTwo" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffTwo" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffTwo" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
				<div id="staffThree" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffThree" data-slide-to="0" class="active"></li>
						<li data-target="#staffThree" data-slide-to="1"></li>
						<li data-target="#staffThree" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffThree" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffThree" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
				<div id="staffFour" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffFour" data-slide-to="0" class="active"></li>
						<li data-target="#staffFour" data-slide-to="1"></li>
						<li data-target="#staffFour" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffFour" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffFour" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>


			<!-- REPEAT OF THE CAROUSELS ABOVE WITH DIFFERENT ID'S-->

			<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
				<div id="staffFive" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffFive" data-slide-to="0" class="active"></li>
						<li data-target="#staffFive" data-slide-to="1"></li>
						<li data-target="#staffFive" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffFive" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffFive" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3">
				<div id="staffSix" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffSix" data-slide-to="0" class="active"></li>
						<li data-target="#staffSix" data-slide-to="1"></li>
						<li data-target="#staffSix" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffSix" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffSix" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-0">
				<div id="staffSeven" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffSeven" data-slide-to="0" class="active"></li>
						<li data-target="#staffSeven" data-slide-to="1"></li>
						<li data-target="#staffSeven" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffSeven" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffSeven" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-3 col-lg-offset-0">
				<div id="staffEight" class="carousel staff-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#staffEight" data-slide-to="0" class="active"></li>
						<li data-target="#staffEight" data-slide-to="1"></li>
						<li data-target="#staffEight" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card staff-card-about" style="background:url('images/staff.jpg'); background-size:cover; background-position:center center;">
								<div class="staff-card-body about-card-body">
									<h3>Chriss Thompson</h3>
									<h5>Vice President of Client Strategy</h5>
									<p class="brand-primary-white">If you’re wondering what Chriss does, it’s probably better to ask what doesn’t he do. Chriss is our guy, the guy, and everybody’s guy to put it simply.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="staff-card">
								<div class="staff-hdr">
									<div class="staff-bg"></div>
									<img class="profile-pic" src="images/staff.jpg" alt="staff member">
								</div>
								<div class="staff-card-body">
									<h3 class="brand-primary-mid-c">Arkansas City</h3>
									<h5>2019 N. Summit</h5>
									<a href="">Visit this Branch &rarr;</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#staffEight" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#staffEight" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="callout mt-2 mb-2">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <div class="callout-body p-2 brand-primary">

          <h3 class="brand-primary-light-c fw-800 mb-1 uppercase">Account Finder</h3>

          <h2 class="brand-primary-white mb-03"><b>Which program is right for you?</b>

          <h3 class="fw-500 muddymint-green mb-1">Answer a few short questions and we'll highlight a program that best fits your needs.</h3>

          <a href="/personal-loans" role="button" class="btn btn-brand-on-green btn-lg">Get Started</a>

        </div>

      </div>

    </div>

  </div>

</section>

<?php include('footer.php') ?>