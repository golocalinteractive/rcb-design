<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../favicon.ico">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>RCB Bank | Welcome</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Header nav -->
    <link rel="stylesheet" type="text/css" href="css/component.css" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- font awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- RCB Stylesheet -->
    <link href="style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
      <nav class="nav-wrapper navbar-fixed-top cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
        <div class="cbp-hsinner hidden-sm hidden-xs">
          <div class="logo-wrapper"><a class="logo" href="home.php"><img src="images/logo-horizontal.png"></a></div>
          <ul class="cbp-hsmenu">
            <li class="personal-nav">
              <a class="top-nav top-nav-personal" href="/personal">Personal<i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <div class="cbp-hssubmenu cbp-hssub-rows md-pt-1">
                <div class="container sm-fluid">
                  <div class="row">

                    <!-- desktop nav content -->
                    <div class="col-lg-1 col-lg-offset-1 col-md-1 col-md-offset-0 hidden-xs hidden-sm">
                      <!-- Nav tabs -->
                      <ul class="tablist nav-personal-tablist" role="tablist">
                        <li role="presentation" class="active"><a href="#personalbank" aria-controls="bank" role="tab" data-toggle="tab">Bank</a></li>
                        <li role="presentation"><a href="#personalborrow" aria-controls="borrow" role="tab" data-toggle="tab">Borrow</a></li>
                        <li role="presentation"><a href="#personalinvest" aria-controls="invest" role="tab" data-toggle="tab">Invest</a></li>
                        <li role="presentation"><a href="#personallearn" aria-controls="manage" role="tab" data-toggle="tab">Learn</a></li>
                      </ul>
                    </div>
                    <div class="col-lg-10 col-md-11 hidden-xs hidden-sm">
                      <div class="personal-tab-content tab-content left-aligned md-pb-1">
                        <div role="tabpanel" class="tab-pane fade in active" id="personalbank">
                          <a class="product-icon-wrapper" href="product.php"><span class="product-icon product-icon-checking"></span>Checking</a>
                          <a class="product-icon-wrapper" href="product.php"><span class="product-icon product-icon-savings"></span>Savings</a>
                          <a class="product-icon-wrapper" href="product-detail.php"><span class="product-icon product-icon-atm-debit-card"></span>ATM/Debit Card</a>
                          <a class="product-icon-wrapper" href="product-detail.php"><span class="product-icon product-icon-anytime-banker"></span>Bank Anywhere</a>
                          <a class="product-icon-wrapper" href="product-detail.php"><span class="product-icon product-icon-kids"></span>Kids Corner</a>
                          <a class="product-icon-wrapper" href="product-detail.php"><span class="product-icon product-icon-privacy-security"></span>Privacy &amp; Security</a>
                          <a class="product-icon-wrapper" href="product.php"><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All Products</a>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="personalborrow">
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-personal-loans"></span>Personal Loans</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mortgage"></span>Mortgage</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mortgage-specialists"></span>Mortgage Specialists</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-loan-officers"></span>Loan Officers</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-home-equity"></span>Home Equity</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-overdraft-summer-loan"></span>Summer Loan</a>
                          <a class="product-icon-wrapper" href=""><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All Products</a>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="personalinvest">
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-cd-ira"></span>CD's &amp; IRA's</a>
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-trust-investments"></span>Trust &amp; Investments</a>
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-cesa"></span>CESA <span class="hidden-md">(Coverdell Education Savings Account)<span></a>
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-hsa"></span>HSA <span class="hidden-md">(Health Savings Account)</span></a>
                          <a class="product-icon-wrapper" href=""><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All Products</a>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="personallearn">
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <a class="inline-block ml-1 fw-600" href="learning-center.php">Visit the Learning Center &raquo;</a>
                        </div>
                      </div><!-- /tabcontent -->
                    </div><!-- /col-->
                  </div><!-- /row-->
                </div><!-- /container -->
                <!-- promo -->
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-lg-12 nav-promo brand-primary-mid white-c">
                      Amet vel modi consequatur aperiam sapiente tenetur labore? <a class="brand-primary-c" href="">Learn more.</a>
                    </div>
                  </div>
                </div><!--/promo -->
              </div><!--/ submenu -->
            </li>
            <li class="business-nav">
              <a class="top-nav top-nav-business" href="/business">Business <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <div class="cbp-hssubmenu cbp-hssub-rows md-pt-1">
                <div class="container">
                  <div class="row">

                    <!-- desktop nav content -->
                    <div class="col-lg-1 col-lg-offset-1 col-md-1 col-md-offset-0 hidden-xs hidden-sm">
                      <!-- Nav tabs -->
                      <ul class="tablist nav-business-tablist" role="tablist">
                        <li role="presentation" class="active"><a href="#businessbank" aria-controls="bank" role="tab" data-toggle="tab">Bank</a></li>
                        <li role="presentation"><a href="#businessborrow" aria-controls="borrow" role="tab" data-toggle="tab">Borrow</a></li>
                        <li role="presentation"><a href="#businessinvest" aria-controls="invest" role="tab" data-toggle="tab">Manage</a></li>
                        <li role="presentation"><a href="#businesslearn" aria-controls="manage" role="tab" data-toggle="tab">Learn</a></li>
                      </ul>
                    </div>
                    <div class="col-lg-10 col-md-11 hidden-xs hidden-sm">
                      <div class="business-tab-content tab-content left-aligned md-pb-1">
                        <div role="tabpanel" class="tab-pane fade in active" id="businessbank">
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span>Checking</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-business-savings"></span>Business Savings</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-business-loans"></span>Business Loans</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mobile-deposit"></span>Mobile Deposit</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-brokerage"></span>Brokerage</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-wealth-builder"></span>Wealth Builder</a>
                          <a class="product-icon-wrapper" href=""><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All<br />Products</a>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="businessborrow">
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-personal-loans"></span>Personal Loans</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mortgage"></span>Mortgage</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mortgage-specialists"></span>Mortgage Specialists</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-loan-officers"></span>Loan Officers</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-home-equity"></span>Home Equity</a>
                          <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-overdraft-summer-loan"></span>Summer Loan</a>
                          <a class="product-icon-wrapper" href=""><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All Products</a>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="businessinvest">
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-cd-ira"></span>CD's &amp; IRA's</a>
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-trust-investments"></span>Trust &amp; Investments</a>
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-cesa"></span>CESA <span class="hidden-md">(Coverdell Education Savings Account)<span></a>
                          <a class="product-icon-wrapper large" href=""><span class="product-icon product-icon-hsa"></span>HSA <span class="hidden-md">(Health Savings Account)</span></a>
                          <a class="product-icon-wrapper" href=""><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All Products</a>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="businesslearn">
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <div class="card">
                            <a href=""><img class="img-responsive" src="images/placeholder.jpg"></a>
                            <a href="">A Headline About Something Awesome Here</a>
                          </div>
                          <a class="inline-block ml-1 fw-600" href="learning-center.php">Visit the Learning Center &raquo;</a>
                        </div>
                      </div><!-- /tabcontent -->
                    </div><!-- /col-->



                    <!-- desktop nav content
                    <div class="col-lg-12 hidden-xs hidden-sm left-aligned md-pb-1">
                      <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-checking"></span>Checking</a>
                      <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-business-savings"></span>Business Savings</a>
                      <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-business-loans"></span>Business Loans</a>
                      <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-mobile-deposit"></span>Mobile Deposit</a>
                      <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-brokerage"></span>Brokerage</a>
                      <a class="product-icon-wrapper" href=""><span class="product-icon product-icon-wealth-builder"></span>Wealth Builder</a>
                      <a class="product-icon-wrapper" href=""><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i><br />View All<br />Products</a>
                    </div>-->

                  </div>
                </div><!-- /container -->
                <!-- promo -->
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-lg-12 nav-promo brand-primary brand-primary-light-c">
                      Amet vel modi consequatur aperiam sapiente tenetur labore? <a class="white-c" href="">Learn more.</a>
                    </div>
                  </div>
                </div><!--/promo -->
              </div><!--/ submenu -->
            </li>
            <li>
              <a href="why-rcb-bank.php">Why RCB Bank?</a>
            </li>
            <li>
              <a href="learning-center.php">Learning Center</a>
            </li>
            <li><a href="location-finder.php">Find a Branch/ATM</a></li>
            <li class="contact-mobile"><a href="contact-us.php">Contact Us<span></span></a></li>
          </ul>
          <ul class="list-unstyled right-nav">
            <li class="hidden-md"><a href="contact-us.php">Contact Us</a></li>
            <li class="hidden-md"><a href="#search" role="button" data-toggle="modal" data-target="#search" id="searchAnchor"><i class="fa fa-search" aria-hidden="true"></i></a></li>
            <li><button class="btn sign-in" data-toggle="modal" data-target="#signIn">Sign In</button></li>
          </ul>
        </div><!-- /cbp-inner -->


        <!-- Mobile nav -->
        <div class="container-fluid hidden-md hidden-lg">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
        <button type="button" class="navbar-toggle mobile-navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
        </button>
        <div class="logo-wrapper"><a class="logo" href="home.php"><img src="images/logo-horizontal.png"></a></div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Personal <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu level3">
                <li class="dropdown-submenu">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Bank <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                    <li><a href="product.php">Checking</a></li>
                    <li><a href="product.php">Savings</a></li>
                    <li><a href="product.php">ATM/Debit Card</a></li>
                    <li><a href="product.php">Overdraft Protection</a></li>
                    <li><a href="product.php">Kids Corner</a></li>
                    <li><a href="product.php">Privacy &amp; Security</a></li>
                    <li><a href="product.php">View All Products</a></li>
                  </ul>
                </li>
              </ul>
              <ul class="dropdown-menu level3">
                <li class="dropdown-submenu">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Borrow <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                    <li><a href="product.php">Personal Loans</a></li>
                    <li><a href="product.php">Mortgage</a></li>
                    <li><a href="product.php">Mortgage Specialists</a></li>
                    <li><a href="product.php">Loan Officers</a></li>
                    <li><a href="product.php">Home Equity</a></li>
                    <li><a href="product.php">Summer Loan</a></li>
                    <li><a href="product.php">View All Products</a></li>
                  </ul>
                </li>
              </ul>
              <ul class="dropdown-menu level3">
                <li class="dropdown-submenu">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Invest <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                    <li><a href="product.php">CD's &amp; IRA's</a></li>
                    <li><a href="product.php">Trust &amp; Investments</a></li>
                    <li><a href="product.php">CESA <span class="hidden-md">(Coverdell Education Savings Account)<span></a></li>
                    <li><a href="product.php">HSA <span class="hidden-md">(Health Savings Account)</span></a></li>
                    <li><a href="product.php">View All Products</a></li>
                  </ul>
                </li>
              </ul>
              <ul class="dropdown-menu level3">
                <li class="dropdown-submenu">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Learn<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                    <li><a href="learning-center.php">A Headline About Something Awesome Here</a></li>
                    <li><a href="learning-center.php">A Headline About Something Awesome Here</a></li>
                    <li><a href="learning-center.php">A Headline About Something Awesome Here</a></li>
                    <li><a href="learning-center.php">A Headline About Something Awesome Here</a></li>
                    <li><a href="learning-center.php">A Headline About Something Awesome Here</a></li>
                    <li><a href="learning-center.php">Visit the Learning Center <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                  </ul>
                </li>
              </ul>
              <div class="nav-promo mobile-nav-promo brand-primary-mid white-c">Amet vel modi consequatur aperiam sapiente tenetur labore? <a class="brand-primary-c" href="">Learn more.</a></div>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Business <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="product.php">Checking</a></li>
                <li><a href="product.php">Business Savings</a></li>
                <li><a href="product.php">Business Loans</a></li>
                <li><a href="product.php">Business Leasing</a></li>
                <li><a href="product.php">Mobile Deposit</a></li>
                <li><a href="product.php">Brokerage</a></li>
                <li><a href="product.php">Investments</a></li>
                <li><a href="product.php">Wealth Builder</a></li>
                <li><a href="product.php">View All Products</a></li>
              </ul>
              <div class="nav-promo mobile-nav-promo brand-primary white-c">Amet vel modi consequatur aperiam sapiente tenetur labore? <a class="brand-primary-mid-c" href="">Learn more.</a></div>
            </li>
            <li><a href="why-rcb-bank.php">Why RCB Bank?</a></li>
            <li><a href="learning-center.php">Learning Center</a></li>
            <li><a href="contact-us.php">Contact Us</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        <!-- /mobile nav -->
        <!-- /mobile right nav -->
        <ul class="list-unstyled right-nav hidden-md hidden-lg">
          <li class="hidden-xxs"><a href="contact-us.php">Contact Us</a></li>
          <li class="hidden-xxs"><a href="#search" role="button" data-toggle="modal" data-target="#search" id="searchAnchor"><i class="fa fa-search" aria-hidden="true"></i></a></li>
          <li><button class="btn sign-in mobile-sign-in" data-toggle="modal" data-target="#signIn">Sign In</button></li>
        </ul>
      </nav><!--/ nav-wrapper -->

      <div class="nav-backdrop"></div>

</div>

<main>



