<?php include('header.php'); ?>

<!-- RCB Blog Stylesheet -->
<link href="css/bootstrap-slider.min.css" rel="stylesheet">

<!-- RCB Blog Stylesheet -->
<link href="css/location-finder.css" rel="stylesheet">

<div class="container-fluid">

	<div class="loc-tabs-placeholder"></div>

	<div class="row pt-1 pb-1 hidden-lg hidden-md hidden-sm loc-tabs-wrapper">

		<div class="col-lg-12">
			<!-- Nav tabs -->
			<ul class="nav loc-tabs nav-tabs" role="tablist">
				<li class="list-tab-wrapper active" role="presentation"><a href="#listPanel" aria-controls="listPanel" role="tab" data-toggle="tab"><i class="fa fa-list-ul" aria-hidden="true"></i> List</a></li>
				<li class="map-tab-wrapper" role="presentation"><a href="#mapPanel" aria-controls="mapPanel" role="tab" data-toggle="tab"><i class="fa fa-map-marker" aria-hidden="true"></i> Map</a></li>
	  		</ul>
	  	</div>

	</div>

	<div class="tab-content row loc-content">

		<div role="tabpanel" id="listPanel" class="tab-pane">

			<aside class="loc-sidenav">

				<div class="locations">

					<ul class="list-unstyled locations-list">
						<li class="bg-white pl-1 pr-1 pb-1 locator-wrapper hidden-xs">
							<div class="locator">
								<h4 class="brand-primary-c fw-600 pb-05 hidden-xs">Find a branch near you</h4>
								<h4 class="brand-primary-c fw-500 mb-05 mt-05 hidden-sm hidden-md hidden-lg">Find a branch near you</h4>
								<div class="input-group mt-1 find-a-branch-search">
									<div class="inner-addon left-addon">
										<span class="glyphicon glyphicon-search"></span>
										<input type="text" class="form-control" placeholder="City, State or Zip">
									</div>
									<span class="input-group-btn">
										<button class="btn brand-primary-mid brand-primary-white" type="button">Search</button>
									</span>
								</div><!-- /input-group -->
								<!-- Large button group -->
								<div class="btn-group btn-block mt-1">
								  <button class="btn btn-brand-alt btn-block btn-lg dropdown-toggle btn-location-filter" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    Filter by <span class="glyphicon glyphicon-chevron-down"></span>
								  </button>
								  <ul class="loc-list-dropdown dropdown-menu pb-2">
								  	<h5 class="fw-600 ml-1">Distance</h5>
								  	<li class="pl-1 pr-1"><div class="row"><div class="col-sm-4"><small>10mi</small></div><div class="col-sm-4 center-aligned"><small>50mi</small></div><div class="col-sm-4 right-aligned"><small>100mi</small></div></div></li>
									<li class="pl-1 pr-1"><input id="locationSlider" data-slider-id='locationSlider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/></li>
									<!--<li><span id="locationSliderCurrentSliderValLabel">Current Slider Value: <span id="locationSliderVal">3</span></span></li>-->
									<div class="col-sm-12"><h5 class="fw-600 mt-1">Account Services</h5></div>
									<div class="col-sm-6">
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Checking
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										   Savings
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Home Loans
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Consumer Loans
										  </label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Lost or Stolen Card
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										   Reorder Checks
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Business Services
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    EMV Chip Card
										  </label>
										</div>
									</div>
									<div class="col-sm-12"><h5 class="fw-600 mt-1">ATM Services</h5></div>
									<div class="col-sm-6">
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    No Charge ATM
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Account Balance
										  </label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										    Transfer Funds
										  </label>
										</div>
										<div class="checkbox">
										  <label>
										    <input type="checkbox" value="">
										   Cash Withdrawals
										  </label>
										</div>
									</div>
									<div class="col-sm-6 mt-1"><button class="btn btn-brand-alt btn-block">Apply filters</button></div>
									<div class="col-sm-6 mt-1"><button class="btn btn-brand-alt btn-block">Clear filters</button></div>
								  </ul>
								</div>
							</div>
						</li>
						<li class="breadcrumb-li">
							<ol class="breadcrumb">
							  <li><a href="#">Find a Location</a></li>
							  <li class="active">Oklahoma</li>
							</ol>
						</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">1</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">2</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Blackwell Office</a></li>
<li class="brand-primary-c">1350 W Doolin Ave. 74631</li>
<li><a class="mr-1" href="tel:9183371362">(580) 363-0005<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">3</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Stillwater Office</a></li>
<li class="brand-primary-c">324 S Duck St. 74074</li>
<li><a class="mr-1" href="tel:4053777600">(405) 377-7600<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">4</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Norman Office</a></li>
<li class="brand-primary-c">3151 W Tecumseh Rd #200 73072</li>
<li><a class="mr-1" href="tel:5803630005">(580) 363-0005<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">5</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">6</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">7</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">8</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">9</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">10</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">11</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
<li class="location">
<div class="media">
<div class="media-left media-middle">
<div class="map-dot">12</div>
</div>
<div class="media-body">
<ul class="list-unstyled">
<li><a class="fw-600" href="">Bartlesville Office</a></li>
<li class="brand-primary-c">4224 SE Adams Rd. 74006</li>
<li><a class="mr-1" href="tel:9183371362">(918) 337-1362<a/><a data-toggle="tooltip" data-container="body" data-placement="top" data-html="true" title="<h5 class='m-0 fw-600'>Lobby Hours</h5><p class='brand-primary-white'>Mon – Fri: 9am – 6pm<br />Sat: Closed</p><h5 class='m-0 fw-600'>Drive-Thru Hours</h5><p class='brand-primary-white'>Mon – Fri: 7am – 6pm<br/>Sat: 8am -12pm</p>"><i class="fa fa-clock-o" aria-hidden="true"></i> Hours</a></li>
</ul>
<a class="btn btn-brand-alt-light-grey hidden-xs" href="">Branch Details</a>
<a class="btn hidden-sm hidden-md hidden-lg" href=""><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
</li>
					</ul>

				</div>

			</aside>

		</div>

		<div role="tabpanel" id="mapPanel" class="tab-pane active">

			<h4 class="map-title brand-primary-c m-0 p-1 hidden-xs hidden-sm">Locations in Oklahoma</h4>

			<div id="map"></div>

		</div>

	</div>

</div>

<?php include('footer.php') ?>

<!-- Bootsrtap slider -->
<script src="js/bootstrap-slider.min.js"></script>

<script>

$(window).on('load', function() {

	if( $(window).width() < 768 ) {

		// activate list tab and list

		$('#mapPanel').removeClass('active')
		$('#listPanel').addClass('active')
		$('.list-tab-wrapper').addClass('active')
		$('.map-tab-wrapper').removeClass('active')
		$('.locator').insertAfter(".loc-tabs-placeholder")

		$('.locator').insertAfter(".loc-tabs-placeholder")
	}

	else {
		$('.locator-wrapper').append($('.locator'))
	}

})

/****** Bootstrap slider http://seiyria.com/bootstrap-slider/ */

$("#locationSlider").slider();

// Creates the map

var map, bounds, infowindow, gmarkers = [];

$('#mapPanel, #map').css('height', ($(window).height() - $('#map').offset().top))

$(window).on('load resize', function() {

	// move the map to the listing panel on tablet/large phones

	if( $(window).width() < 991 && $(window).width() > 767) {
		$( '#map' ).insertAfter( ".breadcrumb" );
		// Set the map height
		$('#mapPanel, #map').css('height', '300px')
	}

	// move the map back to the map panel on desktop or small phones

	else {
		$( '#map' ).insertAfter( ".map-title" );
		// Set the map height
		$('#mapPanel, #map').css('height', ($(window).height() - $('#map').offset().top))
	}

	  var currCenter = map.getCenter();
	  google.maps.event.trigger(map, 'resize');
	  map.setCenter(currCenter);

})

$('.map-tab-wrapper').on('shown.bs.tab', function (e) {
	e.target // newly activated tab
	e.relatedTarget // previous active tab
	$('#mapPanel, #map').css('height', ( $(window).height() - $('#map').offset().top ) )
	var currCenter = map.getCenter();
	google.maps.event.trigger(map, 'resize');
	map.setCenter(currCenter);
})

function initMap() {


	var location = [['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519"><b>Bartlesville Office</b></a><br />4224 SE Adams Rd.<br />Bartlesville, OK 74006</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:9183371362">918.337.1362</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519">Get Directions</a></div>', 36.7430386, -95.9312459, 1]];

	var locations = [
        ['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,7z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519"><b>Bartlesville Office</b></a><br />4224 SE Adams Rd.<br />Bartlesville, OK 74006</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:9183371362">(918) 337-1362</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.7430386,-95.9312459,17z/data=!3m1!4b1!4m5!3m4!1s0x87b71247f6d105c3:0x378ea4e88c2762f4!8m2!3d36.7430386!4d-95.9290519">Get Directions</a></div>', 36.7430386, -95.9312459, 1],
		['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.0361452,-97.0993693,7z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0x9861631061616773!8m2!3d36.8120334!4d-97.3005867"><b>Blackwell Office</b></a><br />1350 W Doolin Ave.<br />Blackwell, OK 74631</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:5803630005">(580) 363-0005</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.0361452,-97.0993693,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0x9861631061616773!8m2!3d36.8120334!4d-97.3005867">Get Directions</a></div>', 36.8120334, -97.3005867, 2],
		['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@36.1757256,-96.9835562,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xc6760e3db6180696!8m2!3d36.1180476!4d-97.0630074"><b>Stillwater Office</b></a><br />324 S Duck St.<br />Stillwater, OK 74074</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:4053777600">(405) 377-7600</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@36.1757256,-96.9835562,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xc6760e3db6180696!8m2!3d36.1180476!4d-97.0630074">Get Directions</a></div>', 36.1180476, -97.0630074, 3],
		['<div class="map-info-window"><p><a href="https://www.google.com/maps/place/RCB+Bank/@35.7622672,-97.043981,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xb0d48bd0fc1ba4e8!8m2!3d35.262861!4d-97.4883842"><b>Norman Office</b></a><br />3151 W Tecumseh Rd #200<br />Norman, OK 73072</p><a role="button" class="btn btn-brand-alt btn-map" href="tel:5803630005">(580) 363-0005</a> <a role="button" class="btn btn-brand-alt btn-map" href="https://www.google.com/maps/place/RCB+Bank/@35.7622672,-97.043981,9z/data=!4m8!1m2!2m1!1sRCB+Bank+Oklahoma!3m4!1s0x0:0xb0d48bd0fc1ba4e8!8m2!3d35.262861!4d-97.4883842">Get Directions</a></div>', 35.262861, -97.4883842, 4],


    ];	
    var centerLatLng = {lat: 36.7430386, lng: -95.9312459};
	var mapCanvas = document.getElementById('map');
	var mapOptions = {
		center: centerLatLng,
		zoom: 9,
		mapTypeControl: false,
		disableDefaultUI: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	map = new google.maps.Map(mapCanvas, mapOptions);

	//create empty LatLngBounds object
	bounds = new google.maps.LatLngBounds();

	infowindow = new google.maps.InfoWindow();
	createMarkers(locations,infowindow);

}

function createMarkers(loc,infowin) {

	var marker, i;
	var numberMarkerImg = {
		url: 'http://golocaldev.com/rcb/mocks/images/pin.png',
		size: new google.maps.Size(45, 45),
		scaledSize: new google.maps.Size(36, 36),
		labelOrigin: new google.maps.Point(18, 18)
	};

	for (i = 0; i < loc.length; i++) {
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(loc[i][1], loc[i][2]),
			title: 'Click for more info',
			label: {text: (i+1).toString(), color: '#ffffff' }, // adds a label to the facility marker and changes the text to white
			icon: numberMarkerImg, // adds a custom marker image
			map: map
		});

		//extend the bounds to include each marker's position
  		bounds.extend(marker.position);

		google.maps.event.addListener(marker, 'click', (function (marker, i) {
			return function () {
				infowin.setContent(loc[i][0]);
				infowin.open(map, marker);
			}
		})(marker, i));

		gmarkers.push(marker);
	}

	//now fit the map to the newly inclusive bounds
	map.fitBounds(bounds);

	//(optional) restore the zoom level after the map is done scaling
	var listener = google.maps.event.addListener(map, "idle", function () {
	    map.setZoom(7);
	    google.maps.event.removeListener(listener);
	});
}

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJz0Oxwx75_kS1j3J67KV_FsWsT8kxpFA&callback=initMap"></script>
