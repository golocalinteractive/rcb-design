<?php include('header.php'); ?>

<section class="container-fluid hero brand-gradient-animated why-hero">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

        <h1 class="brand-primary-white">We Build Relationships.</h1>

        <p class="brand-primary-white">RCB Bank strives to be recognised as a leader in customer service, a leader in cutting-edge banking technology and a leader within the communities we serve.

      </div>

    </div>

  </div>

</section>

<nav class="page-nav brand-primary-mid no-box-shadow">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <ul class="list-unstyled" role="tablist">
              <li role="presentation"><button class="btn fw-600 ml-05">Explore &nbsp; <i class="fa fa-angle-right fw-600" aria-hidden="true"></i></button></li>
              <li role="presentation" class="fw-600 explore">Explore <i class="fa fa-angle-right" aria-hidden="true"></i></li>
              <li role="presentation" class="page-nav-nav"><a href="about" data-scrollto=".about" class="scrollto-btn">About RCB Bank</a></li>
              <li role="presentation" class="page-nav-nav"><a href="products" data-scrollto=".products" class="scrollto-btn">Our Products</a></li>
              <li role="presentation" class="page-nav-nav"><a href="service" data-scrollto=".service" class="scrollto-btn">How We Serve You</a></li>
              <li role="presentation" class="page-nav-nav"><a href="learningcenter" data-scrollto=".learningcenter" class="scrollto-btn">Learning Center</a></li>
            </ul>
          </div>
        </div>
    </div>
</nav>

<div class="page-nav-placeholder"></div>

<header class="page-header about">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400">About RCB Bank</h4>

        <h2 class="fw-500 brand-grey-c"><span class="brand-primary-mid-c">Relationships, Community</span> and <span class="brand-primary-mid-c">Boldness</span></h2>

      </div>

    </div>

  </div>

</header>

<section>

	<div class="container-fluid">

		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

        		<p class="brand-primary-c about-statement">The Purpose of RCB Bank is to meet the financial needs of our communities, its businesses and its customers. At the same time, RCB Bank is a responsible citizen and a business leader within the communities we serve.<br /><a role="button" class="btn btn-brand mt-2 btn-lg">News &amp; Announcements</a></p>
        		
			</div>

		</div>

	</div>

</section>

<section class="container-fluid hero hero-why-rcb-carousel" style="background-image:url(images/learning-center-lg.jpg);">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

		<div id="whyRCBCarousel" class="carousel slide" data-ride="carousel" style="background-color:#154734;">

			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#whyRCBCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#whyRCBCarousel" data-slide-to="1"></li>
				<li data-target="#whyRCBCarousel" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<h3>In the Community</h3>
					<p class="pt-1 pb-1">RCB Bank is a community bank. We don't just sponsor an event, we participate. We are sensitive to the needs of our community and we respond accordingly.</p>
					<a href="staff.php" role="button" class="btn btn-brand-on-light-green btn-lg">Meet the Team</a>
				</div>
				<div class="item">
					<h3>Relationships Matter</h3>
					<p class="pt-1 pb-1">Customer Service is our mission. At RCB Bank, we don’t just provide a service for our customers – we build relationships. We are ethical, trustworthy and sincere.</p>
					<a role="button" class="btn btn-brand-on-light-green btn-lg">Find a Branch</a>
				</div>
				<div class="item">
					<h3>Embracing a Bold Approach</h3>
					<p class="pt-1 pb-1">RCB Bank is a bold bank! We embrace technology; employees are empowered and customers are rewarded. We are proud of the service we provide.</p>
					<a role="button" class="btn btn-brand-on-light-green btn-lg">Find a Branch</a>
				</div>

			</div>

		</div>

      </div>

    </div>

  </div>

</section>

<header class="page-header products">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400">Our Products</h4>

        <h2 class="fw-500 brand-grey-c">A Full Range of <span class="brand-primary-mid-c">Products &amp; Services</span></h2>

      </div>

    </div>

  </div>

</header>

<section class="product-wrapper m-0 pl-1 pb-2">

	<div class="container-fluid">

		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<div id="productOne" class="carousel product-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#productOne" data-slide-to="0" class="active"></li>
						<li data-target="#productOne" data-slide-to="1"></li>
						<li data-target="#productOne" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="product-card">
								<div class="product-icon-wrapper" href=""><span class="product-icon product-icon-bank"></span></div>
								<div class="product-card-body mt-2">
									<h3 class="fw-600 brand-primary-mid-c">Bank</h3>
									<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about personal-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Personal Banking</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">Checking<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Savings<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">ATM/Debit Card<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Overdraft Protection<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Kids Corner<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Privacy &amp; Security<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-light-green btn-lg mt-1">All Products</a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about business-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Business Banking</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">Checking<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Business Savings<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Business Loans<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Business Leasing<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Mobile Deposit<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Brokerage<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-green btn-lg">All Products</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#productOne" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#productOne" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<div id="productTwo" class="carousel product-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#productTwo" data-slide-to="0" class="active"></li>
						<li data-target="#productTwo" data-slide-to="1"></li>
						<li data-target="#productTwo" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="product-card">
								<div class="product-icon-wrapper" href=""><span class="product-icon product-icon-borrow"></span></div>
								<div class="product-card-body mt-2">
									<h3 class="fw-600 brand-primary-mid-c">Borrow</h3>
									<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about personal-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Personal Borrowing</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">Personal Loans<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Mortgage<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Mortgage Specialists<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Loan Officers<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Home Equity<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Summer Loan<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-light-green btn-lg mt-1">All Products</a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about business-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Business Borrowing</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">Business Leasing<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Loan Officers<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-green btn-lg">All Products</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#productTwo" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#productTwo" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<div id="productThree" class="carousel product-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#productThree" data-slide-to="0" class="active"></li>
						<li data-target="#productThree" data-slide-to="1"></li>
						<li data-target="#productThree" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="product-card">
								<div class="product-icon-wrapper" href=""><span class="product-icon product-icon-invest"></span></div>
								<div class="product-card-body mt-2">
									<h3 class="fw-600 brand-primary-mid-c">Invest</h3>
									<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about personal-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Personal Investing</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">CD's &amp; IRA's<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Trust &amp; Investments<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">CESA<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">HSA<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-light-green btn-lg mt-1">All Products</a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about business-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Business Investing</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">Trust Services<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Wealth Management<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-green btn-lg">All Products</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#productThree" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#productThree" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<div id="productFour" class="carousel product-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#productFour" data-slide-to="0" class="active"></li>
						<li data-target="#productFour" data-slide-to="1"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="product-card">
								<div class="product-icon-wrapper"><span class="product-icon product-icon-manage"></span></div>
								<div class="product-card-body mt-2">
									<h3 class="fw-600 brand-primary-mid-c">Manage</h3>
									<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about business-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Manage</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">Lock Box<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Remote Desktop Capture<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Merchant Card Services<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Account View<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-green btn-lg mt-1">All Products</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#productFour" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#productFour" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<div id="productFive" class="carousel product-carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class=" carousel-indicators">
						<li data-target="#productFive" data-slide-to="0" class="active"></li>
						<li data-target="#productFive" data-slide-to="1"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="product-card">
								<div class="product-icon-wrapper" href=""><span class="product-icon product-icon-learn"></span></div>
								<div class="product-card-body mt-2">
									<h3 class="fw-600 brand-primary-mid-c">Learn</h3>
									<p>Adipisci aliquam repellendus a eum. Tenetur optio ea doloremque. Nesciunt eaque rerum ut dolorum ad id debitis. Soluta asperiores nemo eos facere excepturi eos atque.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-card product-card-about personal-card" style="background:url('images/staff-placeholder.jpg'); background-size:cover; background-position:center center;">
								<div class="product-card-body about-card-body pt-2">
									<h3 class="fw-400">Learn</h3>
									<ul class="list-unstyled mt-2">
										<li><a class="fw-600 brand-primary-white" href="">About RCB Bank<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Our Products<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">How We Serve You<a/></li>
										<li><a class="fw-600 brand-primary-white" href="">Why RCB Bank?<a/></li>
									</ul>
									<a role="button" class="btn btn-brand-on-light-green btn-lg mt-1">Learning Center</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#productFive" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#productFive" role="button" data-slide="next">
					<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="callout mt-2 mb-2">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <div class="callout-body p-2 brand-primary">

          <h3 class="brand-primary-light-c fw-800 mb-1 uppercase">Account Finder</h3>

          <h2 class="brand-primary-white mb-03"><b>Which program is right for you?</b>

          <h3 class="fw-500 muddymint-green mb-1">Answer a few short questions and we'll highlight a program that best fits your needs.</h3>

          <a href="/personal-loans" role="button" class="btn btn-brand-on-green btn-lg">Get Started</a>

        </div>

      </div>

    </div>

  </div>

</section>

<header class="page-header service">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400">How We Serve You</h4>

        <h2 class="fw-500 brand-grey-c"><span class="brand-primary-mid-c">Privacy</span> &amp; <span class="brand-primary-mid-c">Security</span></h2>

      </div>

    </div>

  </div>

</header>

<div id="faq" class="bg-md-dk-blue brand-primary">

  <div class="container-fluid">

    <div class="row">

      <div class="col-xs-12 pl-0 pr-0">

        <div id="faqAccordion" data-children=".panel">
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion1" class="faq-a white pt-1 pb-1" aria-expanded="true" aria-controls="faqAccordion1"><span class="pb-2"><div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Advanced Security IT</span></a>
            <div id="faqAccordion1" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion2" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion2">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Regulatory Oversight
            </a>
            <div id="faqAccordion2" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, <a href="#">account finder</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion3" class=" faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion3">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Data SSL Encryption
            </a>
            <div id="faqAccordion3" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion4" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion4">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Secure Web Access
            </a>
            <div id="faqAccordion4" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion5" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion5">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Account Login Failure Lockout
            </a>
            <div id="faqAccordion5" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum <a href="facility.php" href="">Facility page</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion6" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion6">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Double Layer PIN/Token
            </a>
            <div id="faqAccordion6" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum <a href="facility.php" href="">Facility page</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel light-bdr-bottom">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion7" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion7">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Email Best Practices
            </a>
            <div id="faqAccordion7" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum <a href="facility.php" href="">Facility page</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>
          <div class="panel">
            <a data-toggle="collapse" data-parent="#faqAccordion" href="#faqAccordion8" class="faq-a white pt-1 pb-1" aria-expanded="false" aria-controls="faqAccordion8">
                <div class="accordion-plus-minus-btn"><span class="accordion-plus-minus"></span></div> Terms of Use
            </a>
            <div id="faqAccordion8" class="collapse" role="tabpanel">
              <p class="mb-3 mr-2 fw-600 brand-primary-white">
                Donec at ipsum dignissim, rutrum <a href="facility.php" href="">Facility page</a>. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
            </div>
          </div>

        </div>

      </div>

    </div>

  </div><!--/ container -->

</div><!-- /FAQ -->

<header class="page-header service">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400">How We Serve You</h4>

        <h2 class="fw-500 brand-grey-c"><span class="brand-primary-mid-c">Accessibility</span> Standards</span></h2>

      </div>

    </div>

  </div>

</header>

<section class="categories why-page-categories pb-2">

  <div class="container-fluid">

    <div class="row flex">

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

        <div class="category-product mt-2 mb-2">

          <div class="category-product-body">

            <h4 class="brand-primary-mid-c"> <i class="fa fa-universal-access fa-2x" aria-hidden="true"></i> ADA Compliance</h4>

            <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

          </div>

        </div>

      </div><!--/col -->


      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

        <div class="category-product mt-2 mb-2">

          <div class="category-product-body">

            <h4 class="brand-primary-mid-c"><i class="fa fa-volume-up fa-2x" aria-hidden="true"></i>Screen Reader Optimized</h4>

            <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

          </div>

        </div>

      </div><!--/col -->


      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

        <div class="category-product mt-2 mb-2">

          <div class="category-product-body">

            <h4 class="brand-primary-mid-c"><i class="fa fa-mobile fa-2x" aria-hidden="true"></i> Mobile Friendly</h4>

            <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

          </div>

        </div>

      </div><!--/col -->


      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

        <div class="category-product mt-2 mb-2">

          <div class="category-product-body">

            <h4 class="brand-primary-mid-c"><i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i> Modern Web Standards</h4>

            <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

          </div>

        </div>

      </div><!--/col -->


      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

        <div class="category-product mt-2 mb-2">

          <div class="category-product-body">

            <h4 class="brand-primary-mid-c"><i class="fa fa-laptop fa-2x" aria-hidden="true"></i> Cross-Browser Compatibility</h4>

            <p>Non debitis quas illo sed. Ab laudantium ut quas consequatur sint cumque. Sed aliquid nulla consectetur quam. Dolorem mollitia qui sunt quaerat voluptatem reprehenderit eius qui.</p>

          </div>

        </div>

      </div><!--/col -->

      <div class="col-xs-12">

		<a href="" role="button" class="btn btn-brand-alt mt-1">Report an Issue</a>

      </div>

    </div>

  </div>

</section>

<header class="page-header learningcenter">

  <div class="container-fluid">

    <div class="row">

      <div class="col-lg-12">

        <h4 class="mt-0 mb-05 fw-400">Learning Center</h4>

        <h2 class="fw-500 brand-grey-c">Helping You <span class="brand-primary-mid-c">Build Wealth</span></h2>

      </div>

    </div>

  </div>

</header>

<section class="learning-center mt-1 mb-1">

  <div class="container-fluid">

    <div class="row flex mt-1">

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 pb-1">
        <div class="card">
          <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
          <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
        </div>
      </div>

    </div><!-- /row -->

    <div class="row">

      <div class="col-lg-12">

        <h4><a class="brand-primary-mid-c" href="/learing-center">Visit the Learning Center &raquo;</a></h4>

      </div>

    </div>

  </div>

</section>



<?php include('footer.php') ?>
