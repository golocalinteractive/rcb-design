<?php include('header.php'); ?>

<section class="container-fluid hero hero-kids-corner">

  <div class="overlay"></div>

  <div class="row">

    <div class="col-lg-12">

      <div class="hero-content">

        <h5><div class="hero-icon"><span class="product-icon product-icon-kids"></span></div>Kids &amp; Money</h5>

        <h1><a class="brand-primary-white" href="blog.php">Money Saved is Money For Another Day</a></h1>

        <p>Everybody wants to save money, but few do. <a class="brand-primary-light-c" href="blog.php">Learn how to save now</a>, so you have money when you really want it.</p>

      </div>

    </div>

  </div>

</section>

<nav class="page-nav brand-primary-mid">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <ul class="list-unstyled" role="tablist">
                  <li role="presentation"><button class="btn fw-600 ml-05">Explore &nbsp; <i class="fa fa-angle-right fw-600" aria-hidden="true"></i></button></li>
                  <li role="presentation" class="fw-600 explore">Explore <i class="fa fa-angle-right" aria-hidden="true"></i></li>
                  <li role="presentation" class="page-nav-nav"><a href="blog.php">Financial Basics</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="blog.php">Saving &amp; Investing</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="blog.php">Auto &amp; Lending</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="blog.php">ID Theft &amp; Fraud</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="blog.php">Financial Recovery</a></li>
                  <li role="presentation" class="page-nav-nav"><a href="blog.php">Financial Planning</a></li>
                  <li role="presentation" class="page-nav-nav active"><a href="blog.php">Kids &amp; Money</a></li>
            </ul>
          </div>
        </div>
    </div>
</nav>

<div class="page-nav-placeholder"></div>

<section class="blog-category">

	<div class="container-fluid mt-4">

		<div class="row flex mt-1">

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		</div><!-- /row -->

	</div>

</section>

<section class="callout mt-2 mb-2">

	<div class="container-fluid">

        <div class="row">

 			<div class="col-lg-12">

	            <div class="callout-body p-2 brand-primary">

	              <h3 class="brand-primary-light-c fw-800 mb-1 uppercase">Kids &amp; Money</h3>

	              <h2 class="brand-primary-white mb-03"><b>Six Money Tips Teens Need to Know Before They Leave Home</b>

	              <h3 class="fw-500 muddymint-green mb-1">Is your child leaving the nest? Do they fully understand the daily financial issues that will soon become their reality?</h3>

	              <a href="/personal-loans" role="button" class="btn btn-brand-on-green btn-lg">Read more</a>

	            </div>

			</div>

        </div>

	</div>

</section>

<section class="blog-category">

	<div class="container-fluid">

		<div class="row flex">

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2">
		    <div class="card">
		      <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a>
		      <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4>
		    </div>
		  </article>

		  <button class="btn btn-brand-alt btn-lg load">Load More</button>

		</div><!-- /row -->

	</div>

</section>

<?php include('footer.php') ?>

<script>

	var stories = '<article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article> <article class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xxs-12 mb-2"> <div class="card"> <a href=""><img class="img-responsive" src="images/placeholder_md.jpg"></a> <h4 class="fw-500"><a href="">A Headline About Something Awesome Will Go Here Shortly</a></h4> </div></article>';

	$('.load').click(function() {
		$(stories).insertBefore(this);
	})
	
</script>
